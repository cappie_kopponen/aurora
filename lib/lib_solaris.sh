#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   lib_solaris.sh
# Encoding:    UTF-8
# Description: Library supporting Solaris systems
#

##
# Launcher for Solaris utilities (all versions)
#
solaris() {
  if [ "$__SYSTEM" = "solaris" ]; then
    RET=2
    solaris() {
      if [ "$1" = "v11" ] || [ "$1" = "v10" ]; then
        echo "$@" | grep -q -E "\<v$__VERSION\>"
        ret=$?
        if [ $ret -ne 0 ]; then
          return $RET
        fi
      fi
      RET=2
      "$@"
      RET=$?
      return $RET
    }
    solaris $@
  else
    solaris() {
      :
    }
  fi
}

##
# Extension of launcher for Solaris in version "11"
#
v11() {
  if [ "$__VERSION" = "11" ] || [ "$1" = "v10" ]; then
    if [ "$__VERSION" = "11" ]; then
      while [ "$1" = "v10" ]; do
        shift
      done
    fi
    "$@"
  fi
}

##
# Extension of launcher for Solaris in version "10"
#
v10() {
  if [ "$__VERSION" = "10" ] || [ "$1" = "v11" ]; then
    if [ "$__VERSION" = "10" ]; then
      while [ "$1" = "v11" ]; do
        shift
      done
    fi
    "$@"
  fi
}

# The end of the file lib_solaris.sh
