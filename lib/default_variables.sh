#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   default_variables.sh
# Encoding:    UTF-8
# Description: Default settings of an audit script
#

##
# Default string for names
#
AUDIT="audit"

##
# Settings of output color
#
RED="\e[0;31m"
GREEN="\e[0;32m"
BLUE="\e[1;34m"
END="\e[0m"

##
# Name of output directory with audit results
#
DIRECTORY="$AUDIT-`date +%Y-%m-%d-%H%M%S`"

##
# Names of output files with results
#
SELF="${0##*/}"
DEBUG_FILE="$DIRECTORY/${SELF%.*}-debug.log"
RESULT_FILE="$DIRECTORY/${SELF%.*}-results.log"
LOG_FILE=
STORE_FILE=

##
# Initialize script counter to zero
#
SCRIPTS_CURR=0

##
# Basic variables
#
VERBOSE=1
WHOAMI=`whoami`
NAME=

##
# Return codes
#
RET=

##
# Directories to search for executable files
#
if [ -d "/usr/ucb" ]; then
  PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/ucb
else
  PATH=/sbin:/bin:/usr/sbin:/usr/bin
fi

# The end of the file default_variables.sh
