#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   functions.sh
# Encoding:    UTF-8
# Description: Implementation of functions for an audit script
#

##
# Aliases
#
if [ -x "/usr/xpg4/bin/grep" ]; then
  alias __grep="/usr/xpg4/bin/grep"
else
  alias __grep=$(command -v grep)
fi
if [ -x "/usr/xpg4/bin/egrep" ]; then
  alias __egrep="/usr/xpg4/bin/egrep"
else
  if command -v egrep >/dev/null 2>&1; then
    alias __egrep=$(command -v egrep)
  else
    alias __egrep="$(command -v grep) -e"
  fi
fi
if [ -x "/usr/bin/nawk" ]; then
  alias awk="/usr/bin/nawk"
fi
if [ -x "/usr/ucb/ps" ]; then
  alias ps="/usr/ucb/ps"
fi

##
# Enabling expanding aliases
#
if command -v shopt >/dev/null 2>&1; then
  shopt -s expand_aliases
fi

##
# Modified grep
#
grep() {
    __grep "$@" 2>/dev/null
}

##
# Modified egrep
#
egrep() {
    __egrep "$@" 2>/dev/null
}

##
# Prints "verbose" messages
#
__mssg() {
  if [ "$tagVerbose" = "setVerbose" ]; then
    __mssg() {
      echo -e "$@"
    }
    __mssg "$@"
  else
    __mssg() {
      :
    }
  fi
}

##
# Check system requirements
#
__sys_requirements() {
  num="$#"
  i=0
  counter=0
  while [ $i -lt $num ]; do
    if command -v "$1" >/dev/null 2>&1; then
      __mssg -n "  $BLUE->$END Checking for $1... "
      __mssg "yes"
    else
      __mssg -n "  $BLUE->$END Checking for $1... "
      __mssg "$REDx$END "
      echo -e "    $RED>$END build: Unmet dependency: Could not found \"$1\" in PATH" 1>&2
      ((counter++))
    fi
    ((i++))
    shift
  done

  if [ $counter -eq 1 ]; then
    __error_gen "A failure occurred in build - $counter missing dependency"
  elif [ $counter -gt 1 ]; then
    __error_gen "A failure occurred in build - $counter missing dependencies"
  fi
}

##
# Set output color
#
__set_color() {
  if [ "$1" = "setNoColor" ]; then
    RED=""
    GREEN=""
    BLUE=""
    END=""
  elif [ -z "$1" ]; then
    :
    # colors are defined in default_variables.sh
  else
    __error_gen "Internal error - setting color"
  fi
}

##
# Clean temporary files
#
__clean() {
  ls | grep -v build | grep -v lib | grep -v README | grep -v share | grep -v src | grep -v .sh$ | xargs rm -rf >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    sudo ls | grep -v build | grep -v lib | grep -v README | grep -v share | grep -v src | grep -v .sh$ | xargs rm -rf >/dev/null 2>&1
  fi
}

##
# Print header
#
__head() {
  printf "%0.s-" {1..80}; echo
  if [ -n "$1" ]; then
    echo -e "$@"
  fi
}

##
# Print header to log file
#
separator() {
  __head "$@" >> $LOG_FILE
}

##
# Packaging outputs
#
__pack_output() {
  if [ "$ARCHIVE" = "setNoArchive" ]; then
    return
  fi
  echo -ne "\r$GREEN==>$END Packaging outputs to \"${SELF%.*}-${DIRECTORY#$AUDIT-}-`hostname`-output.tar\" ... \033[K"
  tar -cvf ${SELF%.*}-${DIRECTORY#$AUDIT-}-`hostname`-output.tar $DIRECTORY >/dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo
    __error_gen "Unable create package. Please archive and send all content of $DIRECTORY directory. Thank you."
  else
    echo "Done."
  fi
}

##
# Prints progressbar
#
__progressbar() {
  if [ "$tagProgressbar" = "noProgressbar" ]; then
    return
  fi
  if [ -z "$1" ]; then
    _curr=1
  else
    _curr=$1
  fi
  if [ -z "$2" ]; then
    _all=1
  else
    _all=$2
  fi
  _percent=$(($_curr*100/$_all*100/100))
  _done=$(($_percent*32/100))
  _left=$((32-$_done))
  _fill=$(printf "%${_done}s")
  _empty=$(printf "%${_left}s")
  printf "[${_fill// /#}${_empty// / }] ${_percent}%%"
}

##
# Prints progress of processing audit
#
progress() {
  printf "  $BLUE->$END (%d/%d) processing %-30s" $((SCRIPTS_CURR+1)) $SCRIPTS_COUNT "$NAME"
  __progressbar $1 $2
  printf "\r"
  sleep 3
}

##
# Testing if command is available on true/false 
#
iscommand() {
  if command -v "$1" >/dev/null 2>&1; then
    return 0
  else
    return 1
  fi
}

##
# Runs commands and saves outputs
#
run() {
  if [ $VERBOSE -ge 1 ]; then
    echo "[$USER `date +%Y%m%d-%H:%M:%S`] $@" >> $LOG_FILE
  fi
  if [ $VERBOSE -eq 3 ]; then
    echo -e "\033[K  $BLUE->$END $NAME \r\t\t\t[DEBUG]\t Running command: $@"
  fi
  eval "$@" 1>> $LOG_FILE 2>/dev/null
  _ret=$?
  if [ $_ret -gt 0 ]; then
    echo "$NAME [$_ret]# $@" >> $DEBUG_FILE
    eval "$@" 2>> $DEBUG_FILE 1>/dev/null
  fi
  echo >> $LOG_FILE
  return $_ret
}

##
# Saves argumets to a log file
#
log() {
  echo "$@" >> $LOG_FILE
  echo >> $LOG_FILE
}

##
# Creates checksum of file and saves it to a log file
#
__crc() {
  if [ -e /usr/bin/sha1sum ]; then
    _tmp="$(/usr/bin/sha1sum $1 2>&1)"
    echo "sha $_tmp" 2>> $LOG_FILE
    return $?
  elif [ -e /usr/bin/md5sum ]; then
    _tmp="$(/usr/bin/md5sum $1 2>&1)"
    echo "md5 $_tmp" 2>> $LOG_FILE
    return $?
  fi
  return 2
}

##
# Prints info message
#
info() {
  if [ $VERBOSE -ge 2 ]; then
    echo -e "\033[K  $BLUE->$END $NAME \r\t\t\t[INFO]\t $1"
  fi
}

##
# Prints error message
#
error() {
  echo "[ERROR] $1" >> $LOG_FILE
  if [ $VERBOSE -ge 2 ]; then
    echo -e "\033[K  $RED->$END $NAME \r\t\t\t[ERROR]\t $@"
  fi
}

##
# Clears and saves a file
#
download_clear() {
  if [ ! -e "$1" ]; then
    echo "[INFO] File \"$1\" not found" >> $LOG_FILE
    if [ $VERBOSE -ge 2 ]; then
      echo -e "\033[K  $BLUE->$END $NAME \r\t\t\t[INFO]\t File \"$1\" not found"
    fi
    return
  fi
  __head "Content of file \"$1\":" >> $STORE_FILES
  __crc $1 >> $STORE_FILES
  if [ $? -eq 0 ]; then
    __head >> $STORE_FILES
  fi
  if [ -f $1 ]; then
    grep -v '^$\|^\s*\#' $1 >> $STORE_FILES 2>&1 
  fi
  if [ $? -eq 0 ]; then
    info "Download \"$1\" successful."
  else
    error "Download \"$1\" failed."
  fi
}

##
# Logs failure
#
fail() {
  echo "[FAIL] $1" >> $LOG_FILE
  if [ $VERBOSE -ge 2 ]; then
    echo -e "\033[K  $RED->$END $NAME \r\t\t\t[FAIL]\t $1"
  fi
  if [ -n "$2" ]; then
    echo -e "$NAME:${2}: $1" >> $RESULT_FILE
  fi
}

##
# Prints success message
#
pass() {
  echo "[OK] $1" >> $LOG_FILE
  if [ $VERBOSE -ge 2 ]; then
    echo -e "\033[K  $RED->$END $NAME \r\t\t\t[OK]\t $1"
  fi
}

##
# Prints debug message
#
debug() {
  if [ $VERBOSE -ge 3 ]; then
    echo -e "\033[K  $BLUE->$END $NAME \r\t\t\t[DEBUG]\t $1"
  fi
}

##
# Saves a file
#
download() {
  if [ ! -e "$1" ]; then
    echo "[INFO] File \"$1\" not found" >> $LOG_FILE
    if [ $VERBOSE -ge 2 ]; then
      echo -e "\033[K  $BLUE->$END $NAME \r\t\t\t[INFO]\t File \"$1\" not found"
    fi
    return
  fi
  __head "Content of file \"$1\":" >> $STORE_FILES
  __crc $1 >> $STORE_FILES
  if [ $? -eq 0 ]; then
    __head >> $STORE_FILES
  fi
  if [ -f $1 ]; then
    awk '{ print $0 }' $1 >> $STORE_FILES 2>&1 
  fi
  if [ $? -eq 0 ]; then
    info "Download \"$1\" successful."
  else
    error "Download \"$1\" failed."
  fi
}

##
# Check if service is enabled
#
isenabled() {
  if [ -z "$1" ]; then
    return 3
  fi
  redhat v4 v5 v6 run service $1 status
  redhat v7       run systemctl -q is-enabled $1
  solaris svcs -v $1 2>/dev/null | grep online >/dev/null 2>&1
  return $?
}

##
# Check if service is installed
#
isinstalled() {
  if [ -z "$1" ]; then
    return 3
  fi
  redhat rpm -q $1 >/dev/null 2>&1
  solaris pkginfo 2>/dev/null | grep -i $1 >/dev/null 2>&1
  return $?
}

##
# Prints help message
#
__help_mssg() {
  printf "%s\n" "$__MSSG"
  exit 0
}

##
# Check system and its version
#
__check_system() {
  if [ -z $tagSystem ]; then
    echo -e "  $BLUE->$END No platform settings, building for all supported systems and its versions..."
  else
    echo -e "  $BLUE->$END Checking runtime platform and version settings..."
    if [ "$tagSystem" = "redhat" ]; then
      if [ ! -z $tagVersion ] && [ "$tagVersion" != "4" ] && [ "$tagVersion" != "5" ] && [ "$tagVersion" != "6" ] && [ "$tagVersion" != "7" ]; then
        __error_gen "Red Hat is not supported in $tagVersion version"
      else
        LIBSYSTEM="./lib/lib_redhat.sh"
        REMSYSTEM="solaris[[:space:]]"
        if [ -z $tagVersion ]; then
          PRINTVER="in version 4, 5, 6 and 7"
        else
          PRINTVER=$tagVersion
        fi
        PRINTSYS="Red Hat $PRINTVER"
        echo -e "  $BLUE->$END Platform is set to Red Hat $tagVersion"
      fi
    elif [ "$tagSystem" = "solaris" ]; then
      if [ ! -z $tagVersion ] && [ "$tagVersion" != "10" ] && [ "$tagVersion" != "11" ]; then
        __error_gen "Solaris is not supported in $tagVersion version"
      else
        LIBSYSTEM="./lib/lib_solaris.sh"
        REMSYSTEM="redhat[[:space:]]"
        if [ -z $tagVersion ]; then
          PRINTVER="in version 10 and 11"
        else
          PRINTVER=$tagVersion
        fi
        PRINTSYS="Solaris $PRINTVER"
        echo -e "  $BLUE->$END Platform is set to Solaris $tagVersion"
      fi
    else
      __error_gen "Unsupported operating system"
    fi
  fi
}

##
# Initialization of audit script
#
__initialization() {
  # Setting up flags
  tagHelp=
  tagClean=
  tagDependency=
  tagColor=
  tagVerbose=
  tagSystem=
  tagVersion=
  tagNoProgressbar=
  while :; do
    case "$1" in
      -h | --help)
        if [ -z $tagHelp ] && [ -z $tagClean ] && [ -z $tagDependency ] && 
           [ -z $tagColor ] && [ -z $tagVerbose ] && [ -z $tagSystem ] && 
           [ -z $tagNoProgressbar ] && [ -z $tagNoArchive ]; then
          tagHelp="displayHelp"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -a | --noarchive)
        if [ -z $tagNoArchive ] && [ -z $tagHelp ]; then
          tagNoArchive="setNoArchive"
          ARCHIVE="setNoArchive"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -p | --noprogressbar)
        if [ -z $tagNoProgressbar ] && [ -z $tagHelp ]; then
          tagNoProgressbar="setNoProgressbar"
          tagProgressbar="noProgressbar"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -c | --clean)
        if [ -z $tagClean ] && [ -z $tagHelp ]; then
          tagClean="scheduleCleaning"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -d | --deps)
        if [ -z $tagDependency ] && [ -z $tagHelp ]; then
          tagDependency="setCheckDependency"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -m | --nocolor)
        if [ -z $tagColor ] && [ -z $tagHelp ]; then
          tagColor="setNoColor"
          __set_color $tagColor
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --system=?*)
        if [ -z $tagSystem ] && [ -z $tagHelp ]; then
          tagSystem="${1#--system=}"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --system=*)
          __error_empty_opt $1
        ;;
      --verbose=?*)
        if [ -z $tagVerbose ] && [ -z $tagHelp ]; then
          tagVerbose="${1#--verbose=}"
          if [ "$tagVerbose" = "1" ] || [ "$tagVerbose" = "2" ] || [ "$tagVerbose" = "3" ]; then
            VERBOSE=$tagVerbose
            tagVerbose="setVerbose"
          else
            __error_gen "Bad verbose option"
          fi
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --verbose=*)
          __error_empty_opt $1
        ;;
      ?*)
        __err_inv_option $1
        ;;
      *)
        break
    esac
  done

  if [ "$tagHelp" = "displayHelp" ]; then
    __help_mssg
  fi

  echo -e "$GREEN==>$END Running audit... (Mon Apr 25 15:08:28 CEST 2016)"

  if [ ! -z $tagSystem ]; then
    tagSystem="$(echo $tagSystem | awk '{print tolower($0)}')"
    tagVersion=${tagSystem#*-}
    tagVersion=${tagVersion%.*}
    tagSystem=${tagSystem%-*}
    if [ "$tagVersion" = "$tagSystem" ]; then
      __error_gen "Version of operating system is not specified"
    fi
    echo -e "$GREEN==>$END Configuring runtime settings..."
    if [ -z $__VERSION ]; then
      __VERSION=$tagVersion
    elif [ "$__VERSION" != "$tagVersion" ]; then
      if [ "$__SYSTEM" = "solaris" ]; then
        __error_gen "Audit script is already builded for platform Solaris in version $__VERSION"
      elif [ "$__SYSTEM" = "redhat" ]; then
        __error_gen "Audit script is already builded for platform Red Hat in version $__VERSION"
      fi
    fi
    if [ -z $__SYSTEM ]; then
      __SYSTEM=$tagSystem
    elif [ "$__SYSTEM" != "$tagSystem" ]; then
      if [ "$__SYSTEM" = "solaris" ]; then
        __error_gen "Audit script is already builded for platform Solaris"
      elif [ "$__SYSTEM" = "redhat" ]; then
        __error_gen "Audit script is already builded for platform Red Hat"
      fi
    fi
    __check_system
  fi

  if [ -z $__SYSTEM ]; then
    __error_gen "Operating system is not specified"
  elif [ -z $__VERSION ]; then
    __error_gen "Version of operating system is not specified"
  fi

  if [ "$tagClean" = "scheduleCleaning" ]; then
    echo -e "$GREEN==>$END Removing existing old audit result file(s)..."
    __clean
  fi

  if [ "$tagDependency" = "setCheckDependency" ]; then
    echo -e "$GREEN==>$END Checking runtime dependencies..."
    __sys_requirements "hostname" "tar"
    __mssg "$GREEN==>$END Checking runtime dependencies: Done."
  elif [ -z $tagDependency ]; then
    __mssg "$GREEN==>$END Skipping checking runtime dependencies..."
  else
    __error_gen "Internal error - checking dependencies"
  fi
}

# The end of the file functions.sh
