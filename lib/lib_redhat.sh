#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   lib_redhat.sh
# Encoding:    UTF-8
# Description: Library supporting Red Hat systems
#

##
# Launcher for Redhat utilities (all versions)
#
redhat() {
  if [ "$__SYSTEM" = "redhat" ]; then
    RET=2
    redhat() {
      if [ "$1" = "v7" ] || [ "$1" = "v6" ] || [ "$1" = "v5" ] || [ "$1" = "v4" ]; then
        echo "$@" | grep -q -E "\<v$__VERSION\>"
        ret=$?
        if [ $ret -ne 0 ]; then
          return $RET
        fi
      fi
      RET=2
      "$@"
      RET=$?
      return $RET
    }
    redhat $@
  else
    redhat() {
      :
    }
  fi
}

##
# Extension of launcher for Red Hat in version "7"
#
v7() {
  if [ "$__VERSION" = "7" ] || [ "$1" = "v6" ] || [ "$1" = "v5" ] || [ "$1" = "v4" ]; then
    if [ "$__VERSION" = "7" ]; then
      while [ "$1" = "v6" ] || [ "$1" = "v5" ] || [ "$1" = "v4" ]; do
        shift
      done
    fi
    "$@"
  fi
}

##
# Extension of launcher for Red Hat in version "6"
#
v6() {
  if [ "$__VERSION" = "6" ] || [ "$1" = "v7" ] || [ "$1" = "v5" ] || [ "$1" = "v4" ]; then
    if [ "$__VERSION" = "6" ]; then
      while [ "$1" = "v7" ] || [ "$1" = "v5" ] || [ "$1" = "v4" ]; do
        shift
      done
    fi
    "$@"
  fi
}

##
# Extension of launcher for Red Hat in version "5"
#
v5() {
  if [ "$__VERSION" = "5" ] || [ "$1" = "v7" ] || [ "$1" = "v6" ] || [ "$1" = "v4" ]; then
    if [ "$__VERSION" = "5" ]; then
      while [ "$1" = "v7" ] || [ "$1" = "v6" ] || [ "$1" = "v4" ]; do
        shift
      done
    fi
    "$@"
  fi
}

##
# Extension of launcher for Red Hat in version "4"
#
v4() {
  if [ "$__VERSION" = "4" ] || [ "$1" = "v7" ] || [ "$1" = "v6" ] || [ "$1" = "v5" ]; then
    if [ "$__VERSION" = "4" ]; then
      while [ "$1" = "v7" ] || [ "$1" = "v6" ] || [ "$1" = "v5" ]; do
        shift
      done
    fi
    "$@"
  fi
}

# The end of the file lib_redhat.sh
