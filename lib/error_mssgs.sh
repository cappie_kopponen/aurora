#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   error_mssgs.sh
# Encoding:    UTF-8
# Description: Error handling functions: print message and return error codes
#

##
# Invalid option
#
__err_inv_option() {
  echo "build: invalid option -- ${1##*-}" 1>&2
  echo "Try 'build --help' for more information." 1>&2
  exit 1
}

##
# Duplicate options
#
__err_dupl_option() {
  echo "build: duplicate options or bad combination" 1>&2
  echo "Try 'build --help' for more information." 1>&2
  exit 1
}

##
# Empty option
#
__error_empty_opt() {
  echo "build: \"$1\" requires a non-empty option" 1>&2
  echo "Try 'build --help' for more information." 1>&2
  exit 1
}

##
# General error
#
__error_gen() {
  echo -e "$RED==> ERROR:$END $1" 1>&2
  echo "    Aborting..." 1>&2
  exit 2
}

# The end of the file error_mssgs.sh
