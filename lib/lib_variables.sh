#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   lib_variables.sh
# Encoding:    UTF-8
# Description: Default variables of the builder
#

##
# Name of output script
#
SCRIPT="$AUDIT-`date +%Y-%m-%d-%H%M%S`.sh"

##
# Path to source audit scripts
#
SPATH="./src"

##
# Array with all audit scripts
#
AUDIT_SRC=($(ls $SPATH | grep '\.sh$'))

##
# Counters of scripts
#
SCRIPTS_COUNT=`ls ${SPATH}/*.sh | wc -l`

##
# System settings
#
LIBSYSTEM="./lib/lib_solaris.sh ./lib/lib_redhat.sh"
REMSYSTEM="###"

##
# Space - formatted output
#
SPACE="  "

##
# Flags for options
#
tagHelp=
tagNoArchive=
tagNoBuild=
tagProgressbar=
tagLaunch=
tagClean=
tagForce=
tagNoDep=
tagColor=
tagSource=
tagSystem=
tagVersion=
tagVerbose=
tagOutput=
src=()

##
# Platform a version for generated script
#
PRINTSYS="Red Hat in version 4, 5, 6, 7 and Solaris in 10 and 11 version"
PRINTVER=""

##
# Help message for generated script
#
MSSG=`awk '{ print $0 }' ./share/help`

# The end of the file lib_variables.sh
