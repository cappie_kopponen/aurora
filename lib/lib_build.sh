#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   lib_build.sh
# Encoding:    UTF-8
# Description: Library of build
#

. ./lib/error_mssgs.sh
. ./lib/functions.sh

##
# Prints usage message
#
__print_help() {
  awk '{ print $0 }' ./share/usage
  exit 0
}

##
# Loading scripts from ./src directory
#
__loading_scripts() {
  echo -e "$GREEN==>$END Retrieving sources..."
  if [ -n "$src" ]; then
    _tag="notfound"
    _error=
    i=0
    while [ $i -lt ${#src[@]} ]; do
      j=0
      while [ $j -lt ${#AUDIT_SRC[@]} ]; do
        if [ "${src[$i]}" = "${AUDIT_SRC[$j]}" ]; then
          echo -e "  $BLUE->$END Found ${src[i]}"
          _tag="found"
        fi
        ((j++))
      done
      if [ "$_tag" = "notfound" ]; then
        echo -e "  $RED->$END Script ${src[i]} not found"
        _error="err_occured"
      elif [ "$_tag" = "found" ]; then
        _tag="notfound"
      fi
      ((i++))
    done
    if [ -z $_error ]; then
      AUDIT_SRC=("${src[@]}")
      SCRIPTS_COUNT=${#AUDIT_SRC[@]}
      __mssg "$GREEN==>$END Retrieving sources: Done."
    elif [ "$_error" = "err_occured" ]; then
      __error_gen "Unable retrieve all sources from $SPATH directory"
    fi
  else
    if [ ${#AUDIT_SRC[@]} -eq 0 ]; then
      __error_gen "Sources in $SPATH directory not found"
    else
      i=0
      while [ $i -lt ${#AUDIT_SRC[@]} ]; do
        echo -e "  $BLUE->$END Found ${AUDIT_SRC[i]}"
        ((i++))
      done
    fi
  fi
}

##
# Prints progressbar with counter and name of processing script
#
__print_progressbar() {
  printf "  $BLUE->$END$SPACE(%d/%d) processing %-30s" $((SCRIPTS_CURR+1)) $SCRIPTS_COUNT "$NAME"
  __progressbar $1 $2
  printf "\r"
}

##
# Handling options: gets options
#
__getopts() {
  while :; do
    case "$1" in
      -h | --help)
        if [ -z $tagHelp ] && [ -z $tagClean ] && [ -z $tagForce ] && [ -z $tagNoDep ] && 
           [ -z $tagColor ] && [ -z $tagVerbose ] && [ -z $tagOutput ] && [ -z $tagSystem ] && 
           [ -z $src ] && [ -z $tagProgressbar ] && [ -z $tagLaunch ] && [ -z $tagSource ] && 
           [ -z $tagNoBuild ] && [ -z $tagNoArchive ]; then
          tagHelp="displayHelp"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -a | --noarchive)
        if [ -z $tagNoArchive ] && [ -z $tagHelp ]; then
          tagNoArchive="setNoArchive"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -o | --nobuild)
        if [ -z $tagNoBuild ] && [ -z $tagHelp ]; then
          tagNoBuild="setNoBuild"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -p | --noprogressbar)
        if [ -z $tagProgressbar ] && [ -z $tagHelp ]; then
          tagProgressbar="noProgressbar"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -r | --run)
        if [ -z $tagLaunch ] && [ -z $tagHelp ]; then
          tagLaunch="scheduleLaunch"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -c | --clean)
        if [ -z $tagClean ] && [ -z $tagHelp ]; then
          tagClean="scheduleCleaning"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -f | --force)
        if [ -z $tagForce ] && [ -z $tagHelp ]; then
          tagForce="setForceMode"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -d | --nodeps)
        if [ -z $tagNoDep ] && [ -z $tagHelp ]; then
          tagNoDep="setNoDependency"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      -m | --nocolor)
        if [ -z $tagColor ] && [ -z $tagHelp ]; then
          tagColor="setNoColor"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --srcpath=?*)
        if [ -z $tagSource ] && [ -z $tagHelp ]; then
          tagSource="${1#--srcpath=}"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --srcpath=*)
          __error_empty_opt $1
        ;;
      --system=?*)
        if [ -z $tagSystem ] && [ -z $tagHelp ]; then
          tagSystem="${1#--system=}"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --system=*)
          __error_empty_opt $1
        ;;
      -v | --verbose)
        if [ -z $tagVerbose ] && [ -z $tagHelp ]; then
          tagVerbose="setVerbose"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --output=?*)
        if [ -z $tagOutput ] && [ -z $tagHelp ]; then
          tagOutput="${1#--output=}"
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      --output=*)
          __error_empty_opt $1
        ;;
      *?.sh)
        if [ -z $tagHelp ]; then
          src+=($1)
        else
          __err_dupl_option $1
        fi
        shift
        ;;
      ?*)
        __err_inv_option $1
        ;;
      *)
        break
    esac
  done

  if [ "$tagHelp" = "displayHelp" ]; then
    __print_help
  fi

  tagSystem="$(echo $tagSystem | awk '{print tolower($0)}')"
  tagVersion=${tagSystem#*-}
  tagVersion=${tagVersion%.*}
  tagSystem=${tagSystem%-*}
  if [ "$tagVersion" = "$tagSystem" ]; then
    tagVersion=""
  fi

  if [ -n "$tagSource" ]; then
    SPATH=$tagSource
    AUDIT_SRC=($(ls $SPATH | grep '\.sh$'))
    SCRIPTS_COUNT=`ls ${SPATH}/*.sh | wc -l`
  fi
}

# The end of the file lib_build.sh
