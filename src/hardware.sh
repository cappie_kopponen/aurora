#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   hardware.sh
# Encoding:    UTF-8
# Description: Hardware information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

################################################################################
### Classic manual audit

# ------------------------------------------------------------------------------
# The lspci command allows you to display information about PCI buses and 
# devices that are attached to them. 
if iscommand 'lspci'; then
  separator "Display information about PCI buses"
  run lspci
fi

# ------------------------------------------------------------------------------
# The lsusb command allows you to display information about USB buses and 
# devices that are attached to them.
if iscommand 'lsusb'; then
  separator "Display information about USB buses"
  run lsusb
fi

# ------------------------------------------------------------------------------
# The lscpu command allows you to list information about CPUs that are present
# in the system, including the number of CPUs, their architecture, vendor, 
# family, model, CPU caches, etc.
if iscommand 'lscpu'; then
  separator "Display information about CPUs"
  run lscpu
fi

# The end of the file hardware.sh
