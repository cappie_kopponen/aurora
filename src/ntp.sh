#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   ntp.sh
# Encoding:    UTF-8
# Description: NTP service
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

NTPD_CONF=/etc/ntp.conf

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# 3.6 Configure Network Time Protocol (NTP)
NTP_RESTICTIONS=`egrep "restrict *-?4? *default" $NTPD_CONF 2>/dev/null`; ret=$?
if [ $ret -eq 1 ] ; then
  fail "NTP restrictions not set" "1"
elif [ $ret -eq 2 ] ; then
  error "Greping $NTPD_CONF failed. NTP probably not installed."
  redhat run rpm -q ntp 2>/dev/null
else
  pass "NTP restrictions set to: \"$NTP_RESTICTIONS\""
fi
NTP_RESTICTIONS=`grep "restrict -6 default" $NTPD_CONF 2>/dev/null`; ret=$?
if [ $ret -eq 1 ]; then
  fail "NTP restrictions for IPv6 not set" "2"
elif [ $ret -eq 0 ] ; then
  pass "NTP restrictions set to: \"$NTP_RESTICTIONS\""
fi
grep "^server" /etc/ntp.conf &>/dev/null; ret=$?
if [ $ret -eq 1 ]; then
  fail "NTP Server not set to run" "3"
elif [ $ret -eq 0 ]; then
  pass "NTP Server set to run"
fi
NTP_RUN_PRIV=`grep "ntp:ntp" /etc/sysconfig/ntpd 2>/dev/null`; ret=$?
if [ $ret -eq 1 ]; then
  fail "NTP Server probably running as unprivileged user: \"$NTP_RUN_PRIV\"" "4"
elif [ $ret -eq 0 ]; then
  pass "NTP Server is running as unprivileged user"
fi

################################################################################
### Classic manual audit

# NTP configuration
separator
if iscommand 'ntpq'; then
  separator "NTP Configuration"
  download_clear $NTPD_CONF

  separator "Print a list of the peers known to the server as well as a summary of their state:"
  run ntpq -p
else
  log "No NTP service detected"
fi

# The end of the file ntp.sh
