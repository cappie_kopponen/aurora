#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   boot.sh
# Encoding:    UTF-8
# Description: Boot information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

BOOT_CFG_PATH=
redhat eval pkg=$(rpm -qa grub* | awk -F"-" '{print $1}' | grep -m1 -E "^grub[0-9]*$" )
redhat eval BOOT_CFG_PATH=$(rpm -ql $pkg | grep "\.cfg" | grep -m1 "boot")

# Default path to grub configuration file 
if [ -z "$BOOT_CFG_PATH" ]; then
  BOOT_CFG_PATH="/boot/${pkg}/grub.cfg"
fi

if [ -f $BOOT_CFG_PATH ]; then
  debug "Configuration file of boot loader: \"$BOOT_CFG_PATH\""

  ################################################################################
  ### Audit against CIS sec policy

  # ------------------------------------------------------------------------------
  # 1.5.1 Set User/Group Owner on /boot/grub2/grub.cfg
  BOOT_OWN=`stat -L -c "%u %g" $BOOT_CFG_PATH 2>/dev/null | egrep "0 0"`
  if [ $? -ne 0 ]; then
    fail "Ownership for \"$BOOT_CFG_PATH\" failed: \"$BOOT_OWN\"" "1"
  else
    pass "Ownership for \"$BOOT_CFG_PATH\" OK"
  fi

  # ------------------------------------------------------------------------------
  # 1.5.2 Set Permissions on /etc/grub.conf
  BOOT_PERM=`stat -L -c "%a" $BOOT_CFG_PATH 2>/dev/null | egrep ".00"`
  if [ $? -ne 0 ]; then
    fail "Permissions for \"$BOOT_CFG_PATH\" failed: `stat -L -c "%a" $BOOT_CFG_PATH 2>&1 `" "2"
  else
    pass "Permissions for \"$BOOT_CFG_PATH\" OK"
  fi

  # ------------------------------------------------------------------------------
  # 1.5.3 Set Boot Loader Password 
  run "grep '^set superusers' $BOOT_CFG_PATH &>/dev/null"; ret1=$?
  run "grep '^password' $BOOT_CFG_PATH &>/dev/null"; ret2=$?
  if [ $ret1 -ne 0 ] || [ $ret2 -ne 0 ]; then
    fail "Boot loader superuser and password not set" "3"
  else
    pass "Boot loader password set"
  fi

  ################################################################################
  ### Classic manual audit

  redhat separator "Look for dmesg errors"
  redhat v7        run journalctl -a -l --dmesg | tail -50
  redhat v4 v5 v6  run dmesg | tail -50
else
  info "Bootloader grub configuration file not found."
  rm -f $LOG_FILE
fi

# The end of the file boot.sh
