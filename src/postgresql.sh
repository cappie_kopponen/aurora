#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   postgresql.sh
# Encoding:    UTF-8
# Description: PostgreSQL
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

PSQL_VERSION=9.1 #TODO

################################################################################
### Classic manual audit

ps aux | grep "postmaster " | grep -v "grep postmaster" &>/dev/null
if [ $? -eq 0 ]; then
  # ------------------------------------------------------------------------------
  # Kernel Resources - 'kernel.shmmax has been configred
  # Kernel Resources for PostgreSQL usage (Expecting > 500MB < 4GB)
  # Managing Kernel Resources - http://www.postgresql.org/docs/9.1/static/kernel-resources.html
  separator "#ID#${__name}#1.1"
  run 'grep "^[^#]*kernel.shmmax[\\s\\t]*=" /etc/sysctl.conf'

  # ------------------------------------------------------------------------------
  # Kernel Resources - 'kernel.shsmall has been configred
  # Kernel Resources for PostgreSQL usage (Expecting > 500MB < 4GB)
  # Managing Kernel Resources - http://www.postgresql.org/docs/9.1/static/kernel-resources.html
  separator "#ID#${__name}#1.2"
  run 'grep "^[^#]*kernel.shmall[\\s\\t]*=" /etc/sysctl.conf'

  # ------------------------------------------------------------------------------
  # Check if postgres user exists
  separator "Checking user postgres"
  run 'grep "^postgres.*" /etc/passwd'
  if [ $? -eq 0 ]; then
    PSQL_USER=`ps aux | grep "postmaster " | awk -F" " '{print $1}'`
  fi

  # ------------------------------------------------------------------------------
  # Show processes
  run 'ps aux | grep $PSQL_USER'

  # ------------------------------------------------------------------------------
  # Check if NTP synchronization is enabled
  separator "#ID#${__name}#1.3"
  run 'grep "^[\\s]*server\\s+{NTP_SERVER}\\s*$" /etc/ntp.conf'

  # ------------------------------------------------------------------------------
  # File Permissions - '/var/lib/pgsql directory'
  separator "#ID#${__name}#1.4"
  run 'ls -ld /var/lib/pgsql /usr/pgsql-* /usr/pgsql-*/bin /var/lib/pgsql-*/data /var/lib/pgsql-*/backups'

  # ------------------------------------------------------------------------------
  # Configuration files permission
  separator "#ID#${__name}#1.5"
  run 'ls -ld /var/lib/pgsql/*/data/postgresql.conf /var/lib/pgsql/*/data/pg_hba.conf /var/log/pgsql'

  # ------------------------------------------------------------------------------
  # No local entries using 'trust' method
  separator "#ID#${__name}#1.6"
  run 'grep "^[^#]*local[\\s\\t]+" /var/lib/pgsql/*/data/pg_hba.conf'
  run 'egrep "[Tt][Rr][Uu][Ss][Tt][\\s\\t]*$" /var/lib/pgsql/*/data/pg_hba.conf'

  # ------------------------------------------------------------------------------
  # No hostnossl entries exist
  separator "#ID#${__name}#1.7"
  run 'egrep "^[^#]*hostnossl" /var/lib/pgsql/*/data/pg_hba.conf'

  # ------------------------------------------------------------------------------
  # No host entries for all databases users and source addresses
  separator "#ID#${__name}#1.8"
  run 'grep "^[^#]*host(ssl|nossl)?[\\s\\t]+" /var/lib/pgsql/*/data/pg_hba.conf'

  # ------------------------------------------------------------------------------
  # SSL certificates
  separator "#ID#${__name}#1.9"
  run 'ls -l /var/lib/pgsql/*/data/server.key'
  run 'ls -l /var/lib/pgsql/*/data/server.crt'

  # ------------------------------------------------------------------------------
  # Existance of /tmp/.s.PGSQL.5432 file for server spoofing
  separator "#ID#${__name}#1.10"
  run 'ls -l /tmp/.s.PGSQL.5432'

  # ------------------------------------------------------------------------------
  # Download ph_hba.conf for further inspection of security
  download_clear /var/lib/pgsql/*/data/pg_hba.conf

else
  info "PostgreSQL not running, skipped"
  rm -f $LOG_FILE
fi

# The end of the file postgresql.sh
