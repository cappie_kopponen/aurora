#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   cron.sh
# Encoding:    UTF-8
# Description: Cron information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

CRON_CONF_PATH="/etc/cron.d"
CRON_DIRS="/etc/crontab \
           /etc/cron.hourly \
           /etc/cron.daily \
           /etc/cron.weekly \
           /etc/cron.monthly \
           /etc/cron.d" # For CRON 04

################################################################################
### Audit against CIS sec policy
# Ommited checks due to redundancy:
# 

# ------------------------------------------------------------------------------
# 6.1.1 Enable anacron Daemon
package=$(isinstalled cronie-anacron)
if [ $? -ne 0 ]; then
  fail "Anacron is not installed" "1"
else
  pass "Anacron is installed: $package"
  # --------------------------------------------------------------------------
  # 6.1.3 Set User/Group Owner and Permission on /etc/anacrontab
  run 'stat -L -c "%a %u %g" /etc/anacrontab | grep ".00 0 0"'; ret=$?
  if [ "$ret" -eq 0 ]; then
    pass "Permissions on anacron are configured correctly"
  else
    ret_stat=$(stat -L -c '%a %U:%G' /etc/anacrontab 2>/dev/null)
    fail "Permissions on anacron are not configured as recommended: \"$ret_stat\"" "2"
  fi
fi

# ------------------------------------------------------------------------------
# 6.1.2 Enable crond Daemon
isenabled crond; CRON_03=$?
if [ $CRON_03 -eq 3 ]; then
  error "Not able to check crond is running"
elif [ $CRON_03 -ne 0 ]; then
  fail "Crond is not enabled" "3"
else
  pass "Crond is enabled"
  # --------------------------------------------------------------------------
  # 6.1.3 Set User/Group Owner and Permission on /etc/anacrontab
  # 6.1.5 Set User/Group Owner and Permission on /etc/cron.hourly
  # 6.1.6 Set User/Group Owner and Permission on /etc/cron.daily
  # 6.1.7 Set User/Group Owner and Permission on /etc/cron.weekly 
  # 6.1.8 Set User/Group Owner and Permission on /etc/cron.monthly
  # 6.1.9 Set User/Group Owner and Permission on /etc/cron.d
  for dir in $(echo $CRON_DIRS); do
    run "stat -L -c '%a %u %g' $dir | grep '.00 0 0'"; ret=$?
    if [ "$ret" -eq 0 ]; then
      pass "Permissions on $dir are configured correctly"
    else
      ret_stat=$(stat -L -c '%a %U:%G' $dir 2>/dev/null)
      fail "Permissions on \"$dir\" are not configured as recommended: \"$ret_stat\"" "4"
    fi
  done
fi

# ------------------------------------------------------------------------------
# 6.1.10 Restrict at Daemon
if [ -f /etc/at.allow ]; then
  run 'stat -L -c "%a %u %g" /etc/at.allow | grep ".00 0 0"'
  if [ $? -ne 0 ]; then
    ret_stat=$(stat -L -c '%a %U:%G' /etc/at.allow 2>/dev/null)
    fail "User restrictions on the file are: \"$ret_stat\"" "5"
  else
    pass "User restrictions on at jobs are set properly"
  fi
  download /etc/at.allow
else
  fail "File /etc/at.allow does not exist. Create it." "6"
fi

run stat -L /etc/at.deny
if [ $? -ne 1 ]; then
  ret_stat=$(stat -L -c '%a %U:%G' /etc/at.deny 2>/dev/null)
  fail "File /etc/at.deny does exist and user restrictions on the file are: \"$ret_stat\"" "7"
  download /etc/at.deny
fi

# ------------------------------------------------------------------------------
# 6.1.11 Restrict at/cron to Authorized Users
if [ -f /etc/cron.allow ]; then
  run 'stat -L -c "%a %u %g" /etc/cron.allow | grep ".00 0 0"'
  if [ $? -ne 0 ]; then
    ret_stat=$(stat -L -c '%a %U:%G' /etc/cron.allow 2>/dev/null)
    fail "User restrictions on the file are: \"$ret_stat\"" "8"
  else
    pass "User restrictions on at jobs are set properly"
  fi
  download /etc/cron.allow
else  
  fail "File /etc/cron.allow does not exist. Create it." "9"
fi

run stat -L /etc/cron.deny
if [ $? -ne 1 ]; then
  ret_stat=$(stat -L -c '%a %U:%G' /etc/cron.deny 2>/dev/null)
  fail "File /etc/cron.deny does exist and user restrictions on the file are: \"$ret_stat\"" "10"
  download /etc/cron.deny
fi

################################################################################
### Classic manual audit

# TODO: Get the file for audit /etc/audit/audit.rules which is default audit
# rules file
separator "CRON of user ${WHOAMI}:"
run "crontab -l"

separator "CRON processes"
run 'ps aux | grep cron'
run 'ps aux | grep atd'

redhat v7  separator "CRON Logs"
redhat v7  run 'journalctl -l -a -u crond.service'
redhat v4 v5 v6  download /var/log/cron

# The end of the file cron.sh
