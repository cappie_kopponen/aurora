#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   pam.sh
# Encoding:    UTF-8
# Description: PAM
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

PAM_SYSTEM_CONF="/etc/pam.d/system-auth"
PAM_SU_CONF="/etc/pam.d/su"

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# 6.3.1 Upgrade Password Hashing Algorithm to SHA-512 
if iscommand 'authconfig'; then
  PAM_01=$(authconfig --test | grep hashing | grep sha512)
  if [ -z "$PAM_01" ]; then
    fail "Password hashing algorithm is not SHA-512" "1"   
  else
    pass "Password hashing algorithm is SHA-512"
  fi
fi

# ------------------------------------------------------------------------------
# 6.3.2 Set Password Creation Requirement Parameters Using pam_cracklib
# This is specific for two modules: pam_cracklib or pam_pwquality
PAM_02_1=$(egrep "^password.*pam_cracklib\.so|pam_pwquality\.so" $PAM_SYSTEM_CONF)
if [ $? -ne 0 ]; then
  fail "No password creation requirement parameters set"
else
  PAM_PWQUALITY="try_first_pass retry minlen dcredit ucredit ocredit lcredit"

  for option in `echo $PAM_PWQUALITY`; do
    echo $PAM_02_1 | grep "$option" &>/dev/null
    if [ $? -eq 0 ]; then
      pass "Option \"$option\" is used in password quality check"
    else
      fail "Option \"$option\" is not used in password quality check" "2"
    fi
  done
  
fi

# ------------------------------------------------------------------------------
# 6.3.3 Set Lockout for Failed Password Attempts (Not Scored)
# ------------------------------------------------------------------------------
# 6.3.4 Limit Password Reuse
PAM_03=$(grep "^password.*remember=." $PAM_SYSTEM_CONF)
if [ $? -ne 0 ]; then
  fail "Password reuse is not limited"
else
  PAM_03_1=$(echo $PAM_03 | sed 's/^.*remember=\([0-9]*\)/\1/')
  if [ -n "$PAM_03_1" ] && [ $PAM_03_1 -ge 5 ]; then
    pass "Password reuse is limited by \"$PAM_03_1\" old passwords"
  else
    fail "Password reuse is not limited: \"$PAM_03_1\"" "3"
  fi
fi

# ------------------------------------------------------------------------------
# 6.4 Restrict root Login to System Console (Not Scored)
# ------------------------------------------------------------------------------
# 6.5 Restrict Access to the su Command
PAM_04_1=$(grep "^auth.*use_uid" $PAM_SU_CONF)
if [ $? -ne 0 ]; then
  fail "Access to su command is not restricted"
else
  PAM_04_2=$(echo $PAM_04_1 | grep "trust")
  if [ $? -eq 0 ]; then
    fail "Access to su command is restricted, but password is not required: $PAM_04_2" 
  else
    pass "Access to su command is restricted to: $(grep wheel /etc/group | awk -F":" '{print $4}')"
  fi
fi

################################################################################
### Classic manual audit

if iscommand 'authconfig'; then run authconfig --test; fi

# The end of the file pam.sh
