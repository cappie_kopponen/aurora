#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   services.sh
# Encoding:    UTF-8
# Description: Services
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
# 3.6 Configure Network Time Protocol (NTP) - see ntp.sh

# ------------------------------------------------------------------------------
# 1.6.1 Restrict Core Dumps
SERVICES_CD=`grep "^[^#]*hard[[:space:]]*core" /etc/security/limits.conf`
if [ $? -ne 0 ]; then
  fail "Core dumps are not restricted" "1"
  download_clear /etc/security/limits.conf
else
  pass "Core dumps restricted: \"$SERVICES_CD\""
fi

# ------------------------------------------------------------------------------
# 1.6.1 Enable Randomized Virtual Memory Region Placement
redhat eval SERVICES_VA=`sysctl kernel.randomize_va_space | awk -F"= *" '{print $2}'`
if [[ $SERVICES_VA -ne 2 ]]; then
  fail "Randomized Virtual Memory is not forced: \"$SERVICES_VA\"" "2"
else
  pass "Randomized Virtual Memory forced"
fi

# ------------------------------------------------------------------------------
# 2.1.12 - 2.1.18 Disable processes
SERVICES_LIST_PRCS="chargen-dgram chargen-stream daytime-dgram daytime-stream \
         echo-dgram echo-stream tcpmux-server"
for process in $(echo $SERVICES_LIST_PRCS); do
  is_disabled=`chkconfig --list $process 2>/dev/null`; ret=$?
  if [ $ret -eq 0 ]; then
    fail "Package \"$process\" is active: \"$is_disabled\"" "3"
  else
    pass "Package \"$process\" is not active"
  fi
done

# ------------------------------------------------------------------------------
# 3.1 Set Daemon umask
SERVICES_D_UMASK=`grep umask /etc/sysconfig/init | awk -F" " '{print $2}'`
if [ "x$SERVICES_D_UMASK" != "x027" ]; then
  fail "Daemon umask not restricted properly: \"$SERVICES_D_UMASK\"" "4"
else
  pass "Daemon umask restricted to \"$SERVICES_D_UMASK\""
fi

# ------------------------------------------------------------------------------
# 3.2 Remove the X Window System
ret1=2
redhat v7        eval SERVICES_GM=`systemctl get-default 2>/dev/null | grep graphical.target 2>/dev/null`; ret1=$?
redhat v4 v5 v6  eval SERVICES_GM=`grep "init:5:" /etc/inittab`; ret1=$?
package=$(isinstalled xorg-x11-server-common); ret2=$?
if [ $ret1 -eq 0 ]; then
  fail "System is booting to graphical mode: \"$SERVICES_GM\"" "5"
else
  pass "System is not "
fi
if [ $ret2 -eq 0 ]; then
  fail "X Window server is installed: \"$is_installed\"" "5"
else
  pass "X Windows server is not installed"
fi

# ------------------------------------------------------------------------------
# 3.3 Disable Avahi Server
# 3.4 Disable Print Server - CUPS (Not Scored)
# 3.8 - 3-15 (Not Scored)
SERVICES_LIST_SRVS="avahi-daemon cups nfslock rpcgssd rpcbind rpcidmapd \
         rpcsvcgssd" # rpc.statd rpc.gssd cupsd rpc.idmapd bind 
                     # vsftpd httpd dovecot samba squid net-snmp
for service in $(echo $SERVICES_LIST_SRVS); do
  redhat v7 systemctl is-enabled $service &>/dev/null
  redhat v4 v5 v6 /sbin/service $service status &>/dev/null
  if [ $? -eq 0 ]; then
    fail "Service \"$service\" is active" "6"
  else
    pass "Service \"$service\" is not active"
  fi
done

# ------------------------------------------------------------------------------
# 3.16 Configure Mail Transfer Agent for Local-Only Mode
SERVICES_25=`netstat -anpt 2>/dev/null | grep -vE "127\.0\.0\.1:25[[:space:]]|::1:25[[:space:]]" | grep ":25[[:space:]]" 2>/dev/null`; ret1=$?
SERVICES_25C=`grep "^[[:space:]]*inet_interfaces[[:space:]]*=" /etc/postfix/main.cf`; ret2=$?
if [ $ret1 -eq 0 ]; then
  fail "Mail Transfer Agent does listen to network" "7"
  log "$SERVICES_25"
  log "Inet listen (configuration option): \"$SERVICES_25C\""
else
  pass "Mail Transfer Agent listens only on local"
fi

################################################################################
### Classic manual audit

separator "Check umask in /etc/init.d"
run 'egrep "umask *[0-7]+" -Inr /etc/init.d'

separator "All services in startup:"
run "chkconfig --list | grep :on"

separator "Useless services, should be uninstalled:"
run "chkconfig --list | grep -v :on"


# ------------------------------------------------------------------------------
# Checkint for all inetd and xinetd processes
if [ -f "/etc/inetd.conf" ]; then download_clear "/etc/inetd.conf"; fi
if [ -f "/etc/xinetd.conf" ]; then download_clear "/etc/xinetd.conf"; fi

# ------------------------------------------------------------------------------
# RPC Info
if iscommand "rpcinfo"; then run "rpcinfo -p localhost"; fi

# ------------------------------------------------------------------------------
# Show status of all services 
redhat v7 separator "List services not-found or failed"
redhat v7 run 'systemctl list-units --type service --all | egrep "not-found|failed"'
redhat v7 separator "List all services"
# For each service unit, this command displays its full name (UNIT) followed by
# a note whether  the unit  has been loaded (LOAD), its high-level (ACTIVE) and 
# low-level (SUB) unit activation state, and a short description (DESCRIPTION).
redhat v7 run systemctl list-units --type service --all

# The end of the file services.sh
