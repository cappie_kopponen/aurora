#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   maintenance.sh
# Encoding:    UTF-8
# Description: Maintenance
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

BANNER_FILES="/etc/motd /etc/issue /etc/issue.net"
BANNER_PERMS="644 0 0"
MAINTANANCE_FILES="/etc/passwd /etc/group"
MAINTANANCE_PERMS="644 0 0"
MAINTANANCE_FILES_0="/etc/shadow /etc/gshadow"
MAINTANANCE_PERMS_0="0 0 0"

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
# 9.1.10 Find World Writable Files - filesystem
# 9.1.11 Find Un-owned Files and Directories - filesystem
# 9.1.12 Find Un-grouped Files and Directories - filesystem
# 9.1.13 Find SUID System Executables
# 9.1.14 Find SGID System Executables

# ------------------------------------------------------------------------------
# 8.1 Set Warning Banner for Standard Login Services
for file in $(echo $BANNER_FILES); do
  file_perms=$(stat -c "%a %u %g" $file 2>/dev/null)
  if [[ -n $file_perms ]] && [[ $file_perms == $BANNER_PERMS ]]; then
    pass "Warning banner of \"$file\" exists and has correct permissions"
  else
    fail "Banner of \"$file\" does not exist or has wrong permissions \"$file_perms\"" "1"
  fi
done

# ------------------------------------------------------------------------------
# 8.2 Remove OS Information from Login Warning Banners
for file in $(echo $BANNER_FILES); do
  BANNER_02=$(egrep '(\\v|\\r|\\m|\\s)' $file)
  if [ $? -eq 0 ]; then
    fail "File \"$file\" contains specific OS information" "2"
    download $file
  else
    pass "File \"$file\" does not contain any specific OS information"
  fi
done

# ------------------------------------------------------------------------------
# 8.3 Set GNOME Warning Banner (Not Scored)
# 9.1.1 Verify System File Permissions (Not Scored)
# ------------------------------------------------------------------------------
# 9.1.2 Verify Permissions on /etc/passwd
# 9.1.5 Verify Permissions on /etc/group
# 9.1.6 Verify User/Group Ownership on /etc/passwd
# 9.1.9 Verify User/Group Ownership on /etc/group
for file in $(echo $MAINTANANCE_FILES); do
  file_perms=$(stat -c "%a %u %g" $file 2>/dev/null)
  if [[ -n $file_perms ]] && [[ $file_perms == $MAINTANANCE_PERMS ]]; then
    pass "File \"$file\" has correct permissions"
  else
    fail "File \"$file\" has wrong permissions: \"$file_perms\"" "4"
  fi
done

# ------------------------------------------------------------------------------
# 9.1.3 Verify Permissions on /etc/shadow
# 9.1.4 Verify Permissions on /etc/gshadow
# 9.1.7 Verify User/Group Ownership on /etc/shadow
# 9.1.8 Verify User/Group Ownership on /etc/gshadow
for file in $(echo $MAINTANANCE_FILES_0); do
  file_perms=$(stat -c "%a %u %g" $file 2>/dev/null)
  if [[ -n $file_perms ]] && [[ $file_perms == $MAINTANANCE_PERMS_0 ]]; then
    pass "File \"$file\" has correct permissions"
  else
    fail "File \"$file\" has wrong permissions: \"$file_perms\"" "4"
  fi
done


# ------------------------------------------------------------------------------
# 9.2.1 Ensure Password Fields are Not Empty
MAINTANANCE_05=$(cat /etc/shadow 2>/dev/null | awk -v ORS="," -F":" '($2 == "" ) {print $1}')
if [ -z $MAINTANANCE_05 ]; then
  pass "No account with empty password"
else
  fail "There are accounts with empty password: \"$MAINTANANCE_05\"" "5"
fi

# ------------------------------------------------------------------------------
# 9.2.2 Verify No Legacy "+" Entries Exist in /etc/passwd File
# 9.2.3 Verify No Legacy "+" Entries Exist in /etc/shadow File
# 9.2.4 Verify No Legacy "+" Entries Exist in /etc/group File
MAINTANANCE_06_FILES="/etc/passwd /etc/shadow /etc/group"
for file in $(echo $MAINTANANCE_06_FILES); do
  entries=$(grep "^\+:" $file | awk -v ORS="," -F":" '{print $1}')
  if [ -z $entries ]; then
    pass "No legacy accounts with \"+\" entries in \"$file\""
  else
    fail "Legacy accounts with \"+\" entries in \"$file\": \"$entries\"" "6"
  fi
done

# ------------------------------------------------------------------------------
# 9.2.5 Verify No UID 0 Accounts Exist Other Than root
MAINTANANCE_07=$(cat /etc/passwd | awk -v ORS="," -F":" '($3 == 0 && $1 != "root") {print $1}')
if [ -z "$MAINTANANCE_07" ]; then
  pass "No UID 0 account exists other than root"
else
  fail "Accounts with UID 0 other than root: \"$MAINTANANCE_07\"" "7"
fi

# ------------------------------------------------------------------------------
# 9.2.6 Ensure root PATH Integrity
if [ "`echo $PATH | grep :: `" != "" ]; then fail "Empty Directory in PATH (::)" "8"; fi
if [ "`echo $PATH | grep :$`" != "" ]; then fail "Trailing : in PATH" "8"; fi
MAINTANANCE_PATH=`echo $PATH | /bin/sed -e 's/::/:/' -e 's/:$//' -e 's/:/ /g'`

for dir in $(echo $MAINTANANCE_PATH); do 
  if [ "$dir" = "." ]; then fail "PATH contains ." "8"; fi; shift; continue
  if [ -d $dir ]; then
    stat -c %A $dir 2>/dev/null | cut -c 6,9 | awk '{if ($1 ~ /w/) exit 1; else exit 0}' &>/dev/null
    if [ $? -eq 1 ]; then
      fail "Write permission set on PATH directory \"$dir\": \"$(stat -c %a $dir)\"" "8"
    fi
    if [ "$(stat %u $dir 2>/dev/null)" != "0" ]; then
      fail "PATH Directory \"$dir\" is not owned by root" "8"
    fi
  else
    fail "PATH record $dir is not a directory" "8"
  fi
done

# ------------------------------------------------------------------------------
# 9.2.7 Check Permissions on User Home Directories
for dir in $(egrep -v "root|halt|sync|shutdown" /etc/passwd | awk -F:\
  '($7 != "/sbin/nologin") { print $6 }'); do
  perm=$(stat -c %a $dir 2>/dev/null)
  if [ -z "$perm" ] || [ $perm -ne 700 ]; then
    fail "Wrong permission set on directory \"$dir\": \"$perm\"" "9"
  fi
done

# ------------------------------------------------------------------------------
# 9.2.8 Check User Dot File Permissions 
# TODO
# ------------------------------------------------------------------------------
# 9.2.9 Check Permissions on User .netrc Files
# TODO

# ------------------------------------------------------------------------------
# 9.2.10 Check for Presence of User .rhosts Files
for dir in $(egrep -v "root|halt|sync|shutdown" /etc/passwd | awk -F:\
  '($7 != "/sbin/nologin") { print $6 }'); do
    if [ ! -h "$dir/.rhosts" -a -f "$dir/.rhosts" ]; then
      fail ".rhosts file in $dir" "12"
    fi
done

# ------------------------------------------------------------------------------
# 9.2.11 Check Groups in /etc/passwd
# TODO

# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

# ------------------------------------------------------------------------------
# 

################################################################################
### Classic manual audit

# The end of the file maintenance.sh
