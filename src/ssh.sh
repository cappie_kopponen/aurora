#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   ssh.sh
# Encoding:    UTF-8
# Description: SSH service
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

SSH_CONF="/etc/ssh/ssh_config"
SSHD_CONF="/etc/ssh/sshd_config"

################################################################################
### Audit against CIS sec policy

SSH_Protocol=2
SSH_LogLevel="INFO"
SSH_X11Forwarding="no"
SSH_MaxAuthTries=4
SSH_IgnoreRhosts="yes"
SSH_HostbasedAuthentication="no"
SSH_PermitRootLogin="no"
SSH_PermitEmptyPasswords="no"
SSH_PermitUserEnvironment="no"
SSH_ClientAlive=300

SSHD_CONF_PERMS="600 0 0"

if [ -f $SSHD_CONF ]; then
  # ------------------------------------------------------------------------------
  # 6.2.1 Set SSH Protocol to 2
  # Default value is 2
  SSH_01=$(grep "^Protocol" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_01" ] || [ "$(echo $SSH_01 | awk '{print $2}')" == $SSH_Protocol ]; then
    pass "SSH is in version 2"
  else
    fail "SSH is not in version 2" "1"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.2 Set LogLevel to INFO
  # Default value is INFO
  SSH_02=$(grep "^LogLevel" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_02" ] || [ "$(echo $SSH_02 | awk '{print $2}')" == $SSH_LogLevel ]; then
    pass "SSH LogLevel is set to INFO"
  else
    fail "SSH LogLevel is set to: \"$SSH_02\"" "2"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.3 Set Permissions on /etc/ssh/sshd_config
  stat -L -c '%a %u %g' $SSHD_CONF 2>/dev/null | grep "$SSHD_CONF_PERMS" &>/dev/null
  if [ $? -eq 0 ]; then
    pass "Permissions on /etc/ssh/sshd_config are set properly"
  else
    SSH_03=$(stat -L -c "%a %U:%G" $SSHD_CONF 2>/dev/null)
    fail "Permissions on /etc/ssh/sshd_config are set to: \"$SSH_03\"" "3"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.4 Disable SSH X11 Forwarding
  # Default value is no
  SSH_04=$(grep "^X11Forwarding" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_04" ] || [ "$(echo $SSH_04 | awk '{print $2}')" == $SSH_X11Forwarding ]; then
    pass "X11 Forwarding is disabled"
  else
    fail "X11 Forwarding is enabled" "4"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.5 Set SSH MaxAuthTries to 4 or Less
  # Default value is 6, that is not sufficient
  SSH_05=$(grep "^MaxAuthTries" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -n "$SSH_05" ] && [ $(echo $SSH_05 | awk '{print $2}') -lt $SSH_MaxAuthTries ]; then
    pass "SSH MaxAuthTries set correctly \"$SSH_05\""
  else
    fail "SSH MaxAuthTries set too high \"$SSH_05\"" "5"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.6 Set SSH IgnoreRhosts to Yes
  # Default value is yes
  SSH_06=$(grep "^IgnoreRhosts" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_06" ] || [ "$(echo $SSH_06 | awk '{print $2}')" == $SSH_IgnoreRhosts ]; then
    pass "The .rhosts files are ignored"
  else
    fail "The .rhosts files are not ignored" "6"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.7 Set SSH HostbasedAuthentication to No
  # Default value is no
  SSH_07=$(grep "^HostbasedAuthentication" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_07" ] || [ "$(echo $SSH_07 | awk '{print $2}')" == $SSH_HostbasedAuthentication ]; then
    pass "Hostbased authentication is disabled"
  else
    fail "Hostbased authentication is enabled" "7"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.7 Set SSH PermitRootLogin to No
  # Default value is no
  SSH_08=$(grep "^PermitRootLogin" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_08" ] || [ "$(echo $SSH_08 | awk '{print $2}')" == $SSH_PermitRootLogin ]; then
    pass "Root login is disabled"
  else
    fail "Root login is permitted: \"$SSH_08\"" "8"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.9 Set SSH PermitEmptyPasswords to No
  # Default value is no
  SSH_09=$(grep "^PermitEmptyPasswords" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_09" ] || [ "$(echo $SSH_09 | awk '{print $2}')" == $SSH_PermitEmptyPasswords ]; then
    pass "Empty passwords are disabled"
  else
    fail "Empty passwords are permitted" "9"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.10 Do Not Allow Users to Set Environment Options
  # Default value is no
  SSH_10=$(grep "^PermitUserEnvironment" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  elif [ -z "$SSH_10" ] || [ "$(echo $SSH_10 | awk '{print $2}')" == $SSH_PermitUserEnvironment ]; then
    pass "Users are not allowed to set environment options"
  else
    fail "Users are allowed to set environment options" "10"
  fi

  # ------------------------------------------------------------------------------
  # 6.2.11 Use Only Approved Cipher in Counter Mode
  SSH_11=$(grep "^Ciphers" $SSHD_CONF)
  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  else
    SSH_11_1=$(echo $SSH_11 | awk '{print $1}' | awk -F"," '{\
          for (i=1; i<=NF;i++) {\
            if ($i !~ /aes/ || $i !~ /ctr/)\
              print 1; exit;\
          } print 0}')
    if [ $SSH_11_1 -eq 0 ]; then
      pass "SSH allowed ciphers are all approved"
    else
      fail "SSH allowed ciphers contain dangerous ciphers: \"$SSH_11\"" "11"
    fi
  fi

  # ------------------------------------------------------------------------------
  # 6.2.12 Set Idle Timeout Interval for User Login
  # Default value for Interval is 0 and for CountMax is 3
  SSH_12_I=$(grep "^ClientAliveInterval" $SSHD_CONF);
  SSH_12_C=$(grep "^ClientAliveCountMax" $SSHD_CONF); 

  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  else
    if [ -z "$SSH_12_I" ]; then SSH_12_I=0; else SSH_12_I=$(echo $SSH_12_I | awk '{print $2}'); fi
    if [ -z "$SSH_12_C" ]; then SSH_12_C=3; else SSH_12_C=$(echo $SSH_12_C | awk '{print $2}'); fi

    SSH_12_A=$(( SSH_12_I * SSH_12_C ))
    if [ $SSH_12_A -gt $SSH_ClientAlive ]; then
      fail "Idle Timeout Interval is set to \"$SSH_12_A\"" "12"
    else
      pass "Idle Timeout Interval is set to \"$SSH_12_A\""
    fi
  fi

  # ------------------------------------------------------------------------------
  # 6.2.13 Limit Access via SSH
  SSH_13_1=$(grep "^AllowUsers" $SSHD_CONF)
  SSH_13_2=$(grep "^AllowGroups" $SSHD_CONF)
  SSH_13_3=$(grep "^DenyUsers" $SSHD_CONF)
  SSH_13_4=$(grep "^DenyGroups" $SSHD_CONF)

  if [ -z "$SSH_13_1" ] && [ -z "$SSH_13_2" ] && [ -z "$SSH_13_3" ] && [ -z "$SSH_13_4" ]; then
    fail "User access is not limited" "13"
  else
    [ -n "$SSH_13_1" ] && pass "User access is limited by: \"$SSH_13_1\""
    [ -n "$SSH_13_2" ] && pass "User access is limited by: \"$SSH_13_2\""
    [ -n "$SSH_13_3" ] && pass "User access is limited by: \"$SSH_13_3\""
    [ -n "$SSH_13_4" ] && pass "User access is limited by: \"$SSH_13_4\""
  fi

  # ------------------------------------------------------------------------------
  # 6.2.14 Set SSH Banner
  SSH_14=$(grep "^Banner" $SSHD_CONF)

  if [ $? -gt 1 ]; then
    error "Error accessing $SSHD_CONF"
  else
    if [ -z "$SSH_14" ]; then
      fail "SSH Banner not specified" "14"
    else
      pass "SSH Banner specified by: \"$SSH_14\""
      download $(echo $SSH_14 | awk '{print $2}')
    fi
  fi

  ################################################################################
  ### Classic manual audit

  # Download configuration of SSH
  download $SSHD_CONF

  # Logs of SSH
  redhat v7 separator "Logs from SSH"
  redhat v7 run journalctl -a -l -u sshd.service
  redhat v4 v5 v6 download /var/log/secure

  # ------------------------------------------------------------------------------
  # Get all authorized_keys of users to check permissions
  separator "Check all authorized keys or wrong permissions"
  awk -F ":" '{print $6}' /etc/passwd | while read homedir; do
    if [ -f ${homedir}/.ssh/authorized_keys ]; then log -n $(ls -la "${homedir}/.ssh/authorized_keys" 2>/dev/null); fi
  done
else
  info "SSH Daemon was not found on the system, skipped"
  rm -f $LOG_FILE
fi

# The end of the file ssh.sh
