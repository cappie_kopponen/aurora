#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   monitoring.sh
# Encoding:    UTF-8
# Description: Monitoring
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

################################################################################
### Classic manual audit

# ------------------------------------------------------------------------------
separator "Show all processes"
run ps aux 

# ------------------------------------------------------------------------------
# The free command provides information about both the physical memory (Mem) and
# swap space (Swap). It displays the total amount of memory (total), as well as
# the amount of memory that is in use (used), free (free), shared (shared), in
# kernel buffers (buffers), and cached (cached).
separator "Show memory status"
redhat v7        run free -lh
redhat v4 v5 v6  run free -l

# ------------------------------------------------------------------------------
# The lsblk command allows you to display a list of available block devices. 
if iscommand 'lsblk'; then
  separator "Display list of available block devices"
  run lsblk
fi

# ------------------------------------------------------------------------------
# For each listed file system, the findmnt command displays the target mount 
# point (TARGET), source device (SOURCE), file system type (FSTYPE), and 
# relevant mount options (OPTIONS).
if iscommand 'findmnt'; then
  separator "Display a list of currently mounted file systems"
  run findmnt
fi

# ------------------------------------------------------------------------------
# For each listed file system, the df command displays its name (Filesystem),
# size (1K-blocks or Size), how much space is used (Used), how much space is 
# still available (Available), the percentage of space usage (Use%), and where
# is the file system mounted (Mounted on).
run df -h

# The end of the file monitoring.sh
