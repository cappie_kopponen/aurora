#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   accounts.sh
# Encoding:    UTF-8
# Description: Accounts information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

ACCOUNTS_LOGIN_DEFS="/etc/login.defs"

PASS_MAX_DAYS=90
PASS_MIN_DAYS=7
PASS_WARN_AGE=

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# 7.1.1 Set Password Expiration Days
# Less then or equal to 90
ACCOUNTS_01=$(grep "^ *PASS_MAX_DAYS" $ACCOUNTS_LOGIN_DEFS | awk -F" " '{print $2}')
if [[ -n $ACCOUNTS_01 ]] && [[ $ACCOUNTS_01 -le $PASS_MAX_DAYS ]]; then
  pass "Password expiration days: \"$ACCOUNTS_01\""
else
  fail "Password expiration days: \"$ACCOUNTS_01\"" "1"
fi

redhat eval ACCOUNTS_02=$(chage --list $WHOAMI | grep "Maximum.*password change" | awk -F": *" '{print $2}')
if [[ -n $ACCOUNTS_02 ]] && [[ $ACCOUNTS_02 -le $PASS_MAX_DAYS ]]; then
  pass "Password expiration days of current user: \"$ACCOUNTS_02\""
else
  fail "Password expiration days of current user: \"$ACCOUNTS_02\"" "2"
fi

# ------------------------------------------------------------------------------
# 7.1.2 Set Password Change Minimum Number of Days
# Set to 7 or more days
ACCOUNTS_03=$(grep "^ *PASS_MIN_DAYS" $ACCOUNTS_LOGIN_DEFS | awk -F" " '{print $2}')
if [[ -n $ACCOUNTS_03 ]] && [[ $ACCOUNTS_03 -ge $PASS_MIN_DAYS ]]; then
  pass "Password min changing days: \"$ACCOUNTS_03\""
else
  fail "Password min changing days: \"$ACCOUNTS_03\"" "3"
fi

# TODO: Chage command is not present on FreeBSD systems
redhat eval ACCOUNTS_04=$(chage --list $WHOAMI | grep "Minimum.*password change" | awk -F": *" '{print $2}')
if [[ -n $ACCOUNTS_04 ]] && [[ $ACCOUNTS_04 -ge $PASS_MIN_DAYS ]]; then
  pass "Password min changing days of current user: \"$ACCOUNTS_04\""
else
  fail "Password min changing days of current user: \"$ACCOUNTS_04\"" "4"
fi

# ------------------------------------------------------------------------------
# 7.1.3 Set Password Expiring Warning Days
# Set to 7 or more days
ACCOUNTS_05=$(grep "^ *PASS_WARN_AGE" $ACCOUNTS_LOGIN_DEFS | awk -F" " '{print $2}')
if [[ -n $ACCOUNTS_05 ]] && [[ $ACCOUNTS_05 -ge $PASS_WARN_AGE ]]; then
  pass "Password expiring warning days: \"$ACCOUNTS_05\""
else
  fail "Password expiring warning days: \"$ACCOUNTS_05\"" "5"
fi

# TODO: Chage command is not present on FreeBSD systems
redhat eval ACCOUNTS_06=$(chage --list $WHOAMI | grep ".*warning.*password expires" | awk -F": *" '{print $2}')
if [[ -n $ACCOUNTS_06 ]] && [[ $ACCOUNTS_06 -ge $PASS_MIN_DAYS ]]; then
  pass "Password expiring warning days of current user: \"$ACCOUNTS_06\""
else
  fail "Password expiring warning days of current user: \"$ACCOUNTS_06\"" "6"
fi

# ------------------------------------------------------------------------------
# 7.2 Disable System Accounts
ACCOUNTS_07=$(grep -v "^\+" /etc/passwd | awk -v ORS="," -F":" '($1!="root" &&\
              $1!="sync" && $1!="shutdown" && $1!="halt" && $3<500 && $7!="/sbin/nologin") {print $1}')
if [[ -z "$ACCOUNTS_07" ]]; then
  pass "Access of default application and system accounts is disabled"
else
  fail "There are system accounts able to login: \"$ACCOUNTS_07\"" "7"
fi

# ------------------------------------------------------------------------------
# 7.3 Set Default Group for root Account
ACCOUNTS_08=$(grep "^root:" /etc/passwd | cut -f4 -d:)
if [[ -n "$ACCOUNTS_08" ]] && [[ $ACCOUNTS_08 -eq 0 ]]; then
  pass "Default group for root account is \"$ACCOUNTS_08\""
else
  fail "Default group for root account is \"$ACCOUNTS_08\"" "8"
fi

# ------------------------------------------------------------------------------
# 7.4 Set Default umask for Users
# TODO: For now it only works for bash
ACCOUNTS_09=$(grep "^umask 077" /etc/bashrc | awk -F" " '{print $2}')
ACCOUNTS_10=$(grep "^umask 077" /etc/profile.d/* | awk -F" " '{print $2}')
if [[ -n "$ACCOUNTS_09" ]] && [[ $ACCOUNTS_09 -eq 077 ]]; then
  pass "Default umask for users in /etc/bashrc is set to \"$ACCOUNTS_09\""
else
  fail "Default umask for users in /etc/bashrc is set to \"$ACCOUNTS_09\"" "9"
fi

if [[ -n "$ACCOUNTS_10" ]] && [[ $ACCOUNTS_10 -eq 077 ]]; then
  pass "Default umask for users in /etc/profile.d is set to \"$ACCOUNTS_10\""
else
  fail "Default umask for users in /etc/profile.d is set to \"$ACCOUNTS_10\"" "10"
fi

# ------------------------------------------------------------------------------
# 7.5 Lock Inactive User Accounts
# Should be set to 35 or less
# TODO: useradd command is not always present on FreeBSD systems
if iscommand 'useradd'; then
  ACCOUNTS_11=$(useradd -D 2>/dev/null | grep "INACTIVE" | awk -F"=" '{print $2}')
  if [[ -n "$ACCOUNTS_11" ]] && [[ $ACCOUNTS_11 -le 35 ]] && [[ $ACCOUNTS_11 -gt 0 ]]; then
    pass "Inactive user accounts are disabled after \"$ACCOUNTS_11\" days"
  else
    fail "User accounts are not disabled or disabled after \"$ACCOUNTS_11\" days" "11"
  fi
fi

################################################################################
### Classic manual audit

# Show currently active users
separator "Currently logged on users:"
redhat run w -f

# ------------------------------------------------------------------------------
# Number of accounts
separator "Number of users: "
run wc -l /etc/passwd

separator "Accounts with UID 0:"
ACCOUNTS_USERS=`awk -F: '{if ($3=="0") print $1}' /etc/passwd`
for acc in $ACCOUNTS_USERS; do log -n $acc; done
#NUMBER=`grep "^[^:]*:[^:]*:0:.*$" /etc/passwd | wc -l`
#log "Number of administrator accounts (UID 0): $NUMBER"

# ------------------------------------------------------------------------------
# NIS and YP accounts
separator "NIS(+)/YP accounts"
run grep '^+' /etc/passwd /etc/group
if iscommand 'ypcat'; then run ypcat passwd; fi
if iscommand 'niscat'; then run niscat passwd; fi

# ------------------------------------------------------------------------------
# Show errors in account management by pwck a grpck
separator "Errors from account management"
if iscommand 'pwck'; then run pwck -r; fi
if iscommand 'grpck'; then run grpck -r; fi

# ------------------------------------------------------------------------------
# Not blocked accounts with passwords
#TODO
#separator "Active accounts with password:"
#for acc in `awk -F: '{if (($2!="*") && ($2!="") && ($2!="!") && ($2!="!!") && ($2!="NP") && ($2!="*LK*")) print $1}' /etc/shadow`; do
# log -n $acc;
#done

# ------------------------------------------------------------------------------
# Show /etc/passwd and /etc/group
download /etc/passwd
download /etc/group
download /etc/pam.d/passwd

# ------------------------------------------------------------------------------
# Show last users in system
separator "Lastlog"
run last -xi
if iscommand 'lastlog'; then
  separator "Lastlog per users"
  run lastlog
fi

separator "PATH variables for user ${WHOAMI}:"
run env

echo "$PATH" | grep "\." &>/dev/null
if [ $? -eq 0 ]; then
  separator "Looking for \".\" in PATH variable"
  fail "Found . in PATH variable: $PATH"
else
  pass "No . in PATH variable"
fi

separator "Default password policy:"
run 'grep "^PASS" /etc/login.defs'
download_clear /etc/login.defs

# ------------------------------------------------------------------------------
# Checking /etc/securetty for pts/[0-9]* entries:
# Entries like pts/[0-9]* will allow programs that use pseudo-terminals (pty)
# and pam_securetty to login into root assuming the allocated pty is one of the 
# ones listed; it's normally a good idea not to include these entries because 
# it's a security risk; it would allow, for instance, someone to login into root
# via telenet, which sends passwords in plaintext (note that pts/[0-9]* is the 
# format for udev which is used in RHEL 5.5; it will be different if using devfs 
# or some other form of device management)
ACC_PTS=`grep "pts/[0-9]*" /etc/securetty 2>/dev/null`; ret=$?
if [ $ret -eq 0 ]; then
  failed "Pseudo-terminals are allowed TTYs"
  log "$ACC_PTS"
 else
  pass "Pseudo-terminals not in TTYs"
fi

# ------------------------------------------------------------------------------
# Download all PAM modules to check the security of authentication
download /etc/pam.d/su
# Look for lines similar to: password    required    pam_pwquality.so retry=3
download /etc/pam.d/passwd
# Then look for lines with minlen, minclass, maxsequence or maxrepeat in below
download /etc/security/pwquality.conf

# And even more files for PAM controling of authentication:
download /etc/pam.d/password-auth
download /etc/pam.d/system-auth
download /etc/securetty

# ------------------------------------------------------------------------------
# Download sudoers file
download /etc/sudoers

# ------------------------------------------------------------------------------
# Decide to show /etc/shadow
#download /etc/shadow

# ------------------------------------------------------------------------------
# Show nsswitch configuration
download_clear /etc/nsswitch.conf

# ------------------------------------------------------------------------------
# Get LDAP client configuration
download_clear /etc/openldap/ldap.conf
download_clear /etc/ldap.conf

# ------------------------------------------------------------------------------
# Get all profiles of users to check permissions
separator "Check all user profiles for wrong permissions"
awk -F ":" '{print $6}' /etc/passwd | while read homedir; do
  if [ -f ${homedir}/.bash_profile ]; then log -n $(ls -la "${homedir}/.bash_profile" 2>/dev/null); fi
  if [ -f ${homedir}/.bashrc ]; then log -n $(ls -la "${homedir}/.bashrc" 2>/dev/null); fi
done

# ------------------------------------------------------------------------------
# Get all permissions of home directories
separator "Check permission of home directories for accounts:"
awk -F ":" '{print $1 " " $6}' /etc/passwd | while read user homedir; do
  if [ -d ${homedir} ]; then log -n $(echo -n "${user}: "; ls -lad "${homedir}" 2>/dev/null); fi
done

################################################################################
### Red Hat 7.x specific

# ------------------------------------------------------------------------------
# Show live root password policy
redhat v7 separator "User ${WHOAMI} live password policy"
redhat v7 run chage -l ${WHOAMI}

# ------------------------------------------------------------------------------
# Show failed login attempts
redhat v7 separator "Failed login attempts per user"
redhat v7 run faillock

# The end of the file accounts.sh
