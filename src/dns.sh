#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   dns.sh
# Encoding:    UTF-8
# Description: DNS information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

NAMED_CONF=/etc/named.conf

################################################################################
### Audit against CIS sec policy
# ------------------------------------------------------------------------------

################################################################################
### Classic manual audit

package=$(isinstalled named)
isinstalled named
ret=$?
if [ $ret -eq 0 ]; then
  info "Named is installed: $package"

  # Named ICS DNS configuration
  download_clear $NAMED_CONF

  # Download all included configuration files
  egrep "^[[:space:]]*include +" $NAMED_CONF 2>/dev/null | awk '{print $2}' | sed 's/[";]*//g' | while read file; do
  if [ -f $file ]; then
    download_clear $file
  else
    error "Named configuration tries to include non-existing file: $file"
  fi
  done

  separator "Review permissions of named binary:"
  NAMED_BIN=`which named 2>/dev/null`
  [ $? -eq 0 ] && run /usr/bin/ls -l $NAMED_BIN

  NAMED_DIR=`egrep "^[[:space:]]*directory[[:space:]]* \"[^\"]*\"[[:space:]]*" $NAMED_CONF 2>/dev/null | sed -n 's/.*\"\(.*\)\".*$/\1/p'`
  if [ -d $NAMED_DIR ]; then
    /usr/bin/ls -l -h -R $NAMED_DIR
:
  else 
    error "Named directory \"$NAMED_DIR\" not a directory"
  fi

else
  info "Named DNS was not found on the system, skipped"
  rm -f $LOG_FILE
fi

# The end of the file dns.sh
