#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   audit.sh
# Encoding:    UTF-8
# Description: Auditing service
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

AUDIT_CONF="/etc/audit/audit.conf" # TODO
AUDIT_RULES="/etc/audit/audit.rules" # TODO

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
# CCE-26657-7 - part of SELinux audit

# ------------------------------------------------------------------------------
# 5.2.1.1 Configure Audit Log Storage Size (Not Scored)
# 5.2.1.2 Disable System on Audit Log Full (Not Scored)
# ------------------------------------------------------------------------------
# 5.2.1.3 Keep All Auditing Information (Scored)
AUDIT_01=$(grep max_log_file_action $AUDIT_CONF | awk -F"= *" '{print $2}')
if [ -n "$AUDIT_01" ] && [ "$AUDIT_01" == "keep_logs" ]; then
  pass "All auditing information is retained"
else
fail "Auditing information are not retained: \"$AUDIT_01\"" "1"
fi

# ------------------------------------------------------------------------------
# 5.2.2 Enable auditd Service
#isenabled auditd; AUDIT_02=$?
if [[ $AUDIT_02 -ne 0 ]]; then
  fail "Auditd is not enabled" "2"
else
  pass "Auditd is enabled"
fi

# ------------------------------------------------------------------------------
# 5.2.3 Enable Auditing for Processes That Start Prior to auditd
# TODO - this will be the hard one (3)

# ------------------------------------------------------------------------------
# 5.2.4 Record Events That Modify Date and Time Information
# 
AUDIT_04_1=$(grep time-change $AUDIT_RULES); ret=$?
if [ $ret -ne 0 ]; then
  fail "Time modification is not audited" "4"
else
  AUDIT_04_2=$(echo "$AUDIT_04_1" | awk -F" " '{if ($0 ~ /settimeofday/) {\
              for (i=1; i<=NF;i++) {\
                if ($i ~ /-a/ && $(i+1) ~ /always/  && $(i+1) ~ /exit/)\
                    print 0; else print 1; exit;\
              }}}')
  AUDIT_04_3=$(echo "$AUDIT_04_1" | awk -F" " '{if ($0 ~ /clock_settime/) {\
              for (i=1; i<=NF;i++) {\
                if ($i ~ /-a/ && $(i+1) ~ /always/  && $(i+1) ~ /exit/)\
                    print 0; else print 1; exit;\
              }}}')
  if [ $AUDIT_04_2 -eq 0 ] && [ $AUDIT_04_3 -eq 0 ]; then
    pass "Time modification is audited correctly"
  else  
    fail "Time modification is not audited" "4"
  fi

  separator "#${__name}:4"
  run grep time-change $AUDIT_RULES # pre istotu na rucnu kontrolu
  separator
fi

# ------------------------------------------------------------------------------
# 5.2.5 Record Events That Modify User/Group Information
AUDIT_05_FILES="/etc/group /etc/passwd /etc/shadow /etc/gshadow /etc/security/opasswd"
for AUDIT_05_FILE in $(echo $AUDIT_05_FILES); do
  if [ ! -f $AUDIT_05_FILE ]; then
    continue
  fi
  egrep "\-w *${AUDIT_05_FILE}.*\-k *" $AUDIT_RULES 2>/dev/null | grep "\-p *wa" &>/dev/null; ret=$?
  if [ $ret -ne 0 ]; then
    fail "Modifications to file \"$AUDIT_05_FILE\" is not recorded" "5"
  else
    pass "Modifications to file \"$AUDIT_05_FILE\" is recorded"
  fi
done

# ------------------------------------------------------------------------------
# 5.2.6 Record Events That Modify the System's Network Environment
AUDIT_06_1=$(grep system-locale $AUDIT_RULES | egrep "sethostname|setdomainname"); ret=$?
if [ $ret -ne 0 ]; then
  fail "Network environment is not audited" "6"
else
  AUDIT_06_2=$(echo "$AUDIT_06_1" | awk -F" " '{
              for (i=1; i<=NF;i++) {\
                if ($i ~ /-a/ && $(i+1) ~ /always/  && $(i+1) ~ /exit/)\
                    print 0; else print 1; exit;\
              }}')
  if [ $AUDIT_06_2 -eq 0 ]; then
    pass "Network environment is audited correctly"
  else
    fail "Network environment is not audited" "6"
  fi

  separator "#${__name}:6"
  run grep system-locale $AUDIT_RULES # manual control
  separator
fi
AUDIT_06_FILES="/etc/issue /etc/issue.net /etc/hosts /etc/sysconfig/network"
for AUDIT_06_FILE in `echo $AUDIT_06_FILES`; do
  if [ ! -f $AUDIT_06_FILE ]; then
    continue
  fi
  egrep "\-w *${AUDIT_06_FILE}.*\-k *" $AUDIT_RULES 2>/dev/null | grep "\-p wa" &>/dev/null; ret=$?
  if [ $ret -ne 0 ]; then
    fail "Modifications to file \"$AUDIT_06_FILE\" is not recorded" "6"
  else
    pass "Modifications to file \"$AUDIT_06_FILE\" is recorded"
  fi
done

# ------------------------------------------------------------------------------
# 5.2.7 Record Events That Modify the System's Mandatory Access Controls
# In a selinux script - rule 2

# ------------------------------------------------------------------------------
# 5.2.8 Collect Login and Logout Events
AUDIT_08_FILES="/var/log/faillog /var/log/lastlog /var/log/tallylog"
for AUDIT_08_FILE in `echo $AUDIT_07_FILES`; do
  if [ ! -f $AUDIT_08_FILE ]; then
    continue
  fi
  egrep "\-w *${AUDIT_08_FILE}.*\-k *" $AUDIT_RULES 2>/dev/null | grep "\-p wa" &>/dev/null; ret=$?
  if [ $ret -ne 0 ]; then
    fail "Login and logout events to \"$AUDIT_08_FILE\" are not recorded" "8"
  else
    pass "Login and logout events to \"$AUDIT_08_FILE\" are recorded"
  fi
done

separator "#${__name}:8"
run grep logins $AUDIT_RULES # manual control
separator

# ------------------------------------------------------------------------------
# 5.2.9 Collect Session Initiation Information
AUDIT_09_FILES="/var/run/utmp /var/log/wtmp /var/log/btmp"
for AUDIT_09_FILE in `echo $AUDIT_09_FILES`; do
  if [ ! -f $AUDIT_09_FILE ]; then
    continue
  fi
  egrep "\-w *${AUDIT_09_FILE}.*\-k *" $AUDIT_RULES &>/dev/null | grep "\-p wa" &>/dev/null; ret=$?
  if [ $ret -ne 0 ]; then
    fail "Session information in \"$AUDIT_09_FILE\" is not recorded" "9"
  else
    pass "Session information in \"$AUDIT_09_FILE\" is recorded"
  fi
done

separator "#${__name}:9"
run grep session $AUDIT_RULES # manual control
separator

# ------------------------------------------------------------------------------
# 5.2.10 Collect Discretionary Access Control Permission Modification Events 
# TODO

# ------------------------------------------------------------------------------
# 5.2.11 Collect Unsuccessful Unauthorized Access Attempts to Files 
# TODO

# ------------------------------------------------------------------------------
# 5.2.12 Collect Use of Privileged Commands
# TODO

# ------------------------------------------------------------------------------
# 5.2.13 Collect Successful File System Mounts
# TODO

# ------------------------------------------------------------------------------
# 5.2.14 Collect File Deletion Events by User
# TODO

# ------------------------------------------------------------------------------
# 5.2.15 Collect Changes to System Administration Scope (sudoers)
egrep "\-w */etc/sudoers.*\-k *" $AUDIT_RULES &>/dev/null | grep "\-p wa"; ret=$?
if [ $ret -ne 0 ]; then
  fail "Changes to system administrator scope is recorded" "15"
else
  pass "Changes to system administrator scope is recorded"
fi

# ------------------------------------------------------------------------------
# 5.2.16 Collect System Administrator Actions (sudolog)
egrep "\-w */var/log/sudo\.log.*\-k *" $AUDIT_RULES &>/dev/null | grep "\-p wa"; ret=$?
if [ $ret -ne 0 ]; then
  fail "System administrator actions are not recorded" "16"
else
  pass "System administrator actions are recorded"
fi


# ------------------------------------------------------------------------------
# 5.2.17 Collect Kernel Module Loading and Unloading
# TODO

# ------------------------------------------------------------------------------
# 5.2.18 Make the Audit Configuration Immutable
run  grep "^-e 2" $AUDIT_RULES; ret=$?
if [ $ret -eq 0 ]; then
  fail "The audit system is not immutable" "16"
else
  pass "The audit system is set immutable"
fi

# ------------------------------------------------------------------------------
# 5.3 Configure logrotate (Not Scored)

################################################################################
### Classic manual audit

# TODO: Get the file for audit /etc/audit/audit.rules which is default audit
# rules file
download_clear $AUDIT_CONF
download_clear $AUDIT_RULES

# The end of the file audit.sh
