#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   filesystem.sh
# Encoding:    UTF-8
# Description: Filesystem
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# 1.1.1, 1.1.5 - 1.1.8 Separate partitions for directories 
FILESYSTEM_SEPARATE_DIRS="/tmp /var /var/tmp /var/log /var/log/audit /home"
for FILESYSTEM_TMP_DIR in `echo $FILESYSTEM_SEPARATE_DIRS`; do
  tmp=`grep "[[:space:]]${FILESYSTEM_TMP_DIR}[[:space:]]" /etc/fstab 2>/dev/null`
  if [ "x${tmp}" == "x" ]; then
    fail "The \"$FILESYSTEM_TMP_DIR\" directory is not on separate partition" "1"
  else
    pass "The \"$FILESYSTEM_TMP_DIR\" directory is on separate partition"
  fi
done

# ------------------------------------------------------------------------------
# 1.1.2 - 1.1.4 Add nodev nosuid and noexec options for /tmp partition
# 1.1.14 - 1.1.16 Add nodev nosuid and noexec options to /dev/shm partition
FILESYSTEM_TMP_PARS="nodev nosuid noexec"
FILESYSTEM_TMP_PARS_DIRS="/tmp /dev/shm"
for FILESYSTEM_TMP_PARS_DIR in `echo $FILESYSTEM_TMP_PARS_DIRS`; do
  FILESYSTEM_TMP=`grep "[[:space:]]${FILESYSTEM_TMP_PARS_DIR}[[:space:]]" /etc/fstab 2>/dev/null`
  if [ $? -ne 0 ]; then
    info "No \"$FILESYSTEM_TMP_PARS_DIR\" in /etc/fstab"
    continue
  fi
  for FILESYSTEM_TMP_PAR in `echo $FILESYSTEM_TMP_PARS`; do
    echo $FILESYSTEM_TMP | grep "$FILESYSTEM_TMP_PAR" &>/dev/null
    if [ $? -ne 0 ]; then
      fail "Missing \"$FILESYSTEM_TMP_PAR\" option on \"$FILESYSTEM_TMP_PARS_DIR\" directory: \"$FILESYSTEM_TMP\"" "2"
    else
      pass "Option \"$FILESYSTEM_TMP_PAR\" on \"$FILESYSTEM_TMP_PARS_DIR\" partition found"
    fi
  done
done

# ------------------------------------------------------------------------------
# 1.1.10 Add nodev Option to /home
grep "[[:space:]]/home[[:space:]]" /etc/fstab 2>/dev/null | grep "nodev" &>/dev/null
if [ $? -ne 0 ]; then
  fail "Missing nodev option on /home directory" "3"
else
  pass "Option nodev on /home partition found"
fi

# ------------------------------------------------------------------------------
# 1.1.11 Add nodev Option to Removable Media Partitions (Not Scored)
# 1.1.12 Add noexec Option to Removable Media Partitions (Not Scored)
# 1.1.13 Add nosuid Option to Removable Media Partitions (Not Scored)

# ------------------------------------------------------------------------------
# 1.1.17 Set Sticky Bit on All World-Writable Directories
WW_D=`df --local -P 2>/dev/null | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 \) 2>/dev/null`
FILESYSTEM_WW_SB=`echo "$WW_D" | xargs -I '{}' find '{}' -xdev -type d \( -perm -1000 \) 2>/dev/null`
if [ "x$FILESYSTEM_WW_SB" != "x" ]; then
  fail "World writable directories without sticky bit set" "4"
else
  pass "No world writable directories without sticky bit set"
fi

# ------------------------------------------------------------------------------
# 1.1.18 Disable Mounting of cramfs Filesystems (Not Scored)
# 1.1.19 Disable Mounting of freevxfs Filesystems (Not Scored)
# 1.1.20 Disable Mounting of jffs2 Filesystems (Not Scored)
# 1.1.21 Disable Mounting of hfs Filesystems (Not Scored)
# 1.1.22 Disable Mounting of hfsplus Filesystems (Not Scored)
# 1.1.23 Disable Mounting of squashfs Filesystems (Not Scored)
# 1.1.24 Disable Mounting of udf Filesystems (Not Scored)
# ------------------------------------------------------------------------------

################################################################################
### Classic manual audit

# ------------------------------------------------------------------------------
separator "Get mount potions:"
run "mount"

# ------------------------------------------------------------------------------
separator "World writable directories:"
for directory in `echo "$WW_D"`; do
  log -n $(ls -ald "$directory" 2>/dev/null;)
done

# ------------------------------------------------------------------------------
download_clear /etc/fstab

# ------------------------------------------------------------------------------
# Get free disk space
separator "Free disk space and mounted partitions"
run "df -h"
# ------------------------------------------------------------------------------
# Get all files on filesystem not owned by anyone
separator "Files without owner"
redhat read FILESYSTEM_NOUSER < <(df --local -P 2>/dev/null | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev \( -nouser \) \; 2>/dev/null)
for file in `echo "$FILESYSTEM_NOUSER"`; do
  log -n $(ls -ald "$file" 2>/dev/null)
done

# ------------------------------------------------------------------------------
# Get all files on filesystem without group
separator "Files without group"
redhat read FILESYSTEM_NOGROUP < <(df --local -P 2>/dev/null | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev \( -nogroup \) \; 2>/dev/null)
for file in `echo "$FILESYSTEM_NOGROUP"`; do
  log -n $(ls -ald "$file" 2>/dev/null)
done

# ------------------------------------------------------------------------------
# Get all SUID files on filesystem 
separator "SUID files"
redhat read FILESYSTEM_SUID < <(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f \( -perm -4000 \) 2>/dev/null)
for file in `echo "$FILESYSTEM_SUID"`; do
  log -n $(ls -ald "$file" 2>/dev/null)
done

# ------------------------------------------------------------------------------
# Get all SGID files on filesystem 
separator "SGID files"
redhat read FILESYSTEM_SGID < <(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f \( -perm -2000 \) 2>/dev/null)
for file in `echo "$FILESYSTEM_SGID"`; do
  log -n $(ls -ald "$file" 2>/dev/null)
done

# ------------------------------------------------------------------------------
# Get all SGID files on filesystem 
separator "World writable files"
redhat read FILESYSTEM_SWID < <(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f \( -perm -o+w \) 2>/dev/null)
for file in `echo "$FILESYSTEM_SWID"`; do
  log -n $(ls -ald "$file" 2>/dev/null)
done

# ------------------------------------------------------------------------------
# Get all SGID files on filesystem 
separator "World executable files"
redhat read FILESYSTEM_SXID < <(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f \( -perm -o+x \) 2>/dev/null)
for file in `echo "$FILESYSTEM_SXID"`; do
  log -n $(ls -ald "$file" 2>/dev/null)
done

# ------------------------------------------------------------------------------
# Get shared NFS directories
download /etc/exports*
download /etc/nfsmount.conf

# The end of the file filesystem.sh
