#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   cobbler.sh
# Encoding:    UTF-8
# Description: Cobbler information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

################################################################################
### Classic manual audit

# Cobbler configuration

# Is cobbler installed?
package=$(isinstalled cobbler)
if [ $? -eq 0 ]; then
  info "Cobbler is installed: \"$package\""
  COBBLER_CONF="/etc/cobbler/settings"
  COBBLER_MODULES="/etc/cobbler/modules.conf"
  COBBLER_USERS="/etc/cobbler/modules.conf"

  download $COBBLER_CONF
  download $COBBLER_MODULES
  download $COBBLER_USERS

  separator "Cobbler should be on separate /var partition"
  df -hT /var/www/cobbler 2>/dev/null

  separator "Status of service"
  redhat v4 v5 v6 /sbin/service cobblerd status
  redhat v7 run systemctl status cobblerd.service
  run cobbler check
else
  info "Cobbler was not found on the system, skipped"
  rm -f $LOG_FILE
fi

# The end of the file cobbler.sh
