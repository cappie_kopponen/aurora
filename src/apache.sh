#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   apache.sh
# Encoding:    UTF-8
# Description: Apache
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
#

################################################################################
### Classic manual audit

APACHE_DIR=/etc/httpd

# ------------------------------------------------------------------------------
# Get processes
separator "Httpd processes:"
run 'ps aux | grep http'

# ------------------------------------------------------------------------------
# Does the directory of apache exist? If not, where to get it?
if [ ! -x $APACHE_DIR ]; then
  APACHE_DIR="";
  # echo "Huston, we got a problem, no apache is runnig !"  
  # TODO
fi

# ------------------------------------------------------------------------------
# Get Apache configuration
if [ -n "$APACHE_DIR" ]; then
  separator "Web server configuration:"
  download_clear $APACHE_DIR/conf/*
fi

# ------------------------------------------------------------------------------
# Get all configuration that is included "Directive Include conf.d/*"
# TODO

# The end of the file apache.sh
