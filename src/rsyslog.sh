#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   rsyslog.sh
# Encoding:    UTF-8
# Description: RSYSLOG
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

RSYSLOG_CONF=/etc/rsyslog.conf # TODO
RSYSLOG_SRVC=rsyslog # TODO

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# 
RSYSLOG_01=$(isinstalled rsyslog)
if [ $? -ne 0 ]; then
  fail "RSYSLOG software is not installed" "1"
  RSYSLOG_IS_INSTALLED=1
else
  pass "RSYSLOG software is installed: $RSYSLOG_01"
  RSYSLOG_IS_INSTALLED=0
fi

if [ $RSYSLOG_IS_INSTALLED -eq 0 ]; then
  # ----------------------------------------------------------------------
  # 5.1.2 Activate the rsyslog Service
  ret=-1
  redhat v7 systemctl is-enabled $RSYSLOG_SRVC &>/dev/null; ret=$?
  redhat v4 v5 v6 /sbin/service $RSYSLOG_SRVC status &>/dev/null; ret=$?
  if [ $ret -eq 0 ]; then
    pass "Service \"$RSYSLOG_SRVC\" is active" "2"
  elif [ $ret -eq -1 ]; then
    debug "Check not compatible with OS version" "(2)"
  else
    fail "Service \"$RSYSLOG_SRVC\" is not active"
  fi

  # ----------------------------------------------------------------------
  # 5.1.3 Configure /etc/rsyslog.conf (Not Scored)
  # ----------------------------------------------------------------------
  # 5.1.4 Create and Set Permissions on rsyslog Log Files
  cat $RSYSLOG_CONF 2>/dev/null | grep -v "^ *[#\$\*].*" | awk -F" " '{print $2}' | sed 's/[;#:].*//g' | grep -v "^ *$" | while read file; do 
  if [ ! -f $file ]; then
    continue
  fi
  perms=`stat -c %a $file 2>/dev/null`
  if [ $perms -gt 600 ]; then
    fail "Log file \"$file\" has wrong permissions: \"$perms\"" "3"
  else
    pass "Log file \"$file\" pass permissions check"
  fi
  done

  # ----------------------------------------------------------------------
  # 5.1.5 Configure rsyslog to Send Logs to a Remote Log Host
  RSYSLOG_04=`grep "^*.*[^I][^I]*@" $RSYSLOG_CONF`; ret=$?
  if [ $ret -ne 0 ]; then
    fail "Logs are not sent to a remote host" "4"
  else
    pass "Logs are sent to a remote host: \"$RSYSLOG_04\""
  fi

  # ----------------------------------------------------------------------
  # 5.1.6 Accept Remote rsyslog Messages Only on Designated Log Hosts (Not Scored)


################################################################################
### Classic manual audit

  # ----------------------------------------------------------------------
  # Get syslog configuration file
  download_clear $RSYSLOG_CONF

################################################################################
else
  rm -f $LOG_FILE
fi

# The end of the file rsyslog.sh
