#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   oracle.sh
# Encoding:    UTF-8
# Description: Oracle
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
#

################################################################################
### Classic manual audit

ORACLE_LOGIN="oracle"
ORACLE_HOME=`env | grep "^ORACLE_HOME=" | awk -F"=" '{print $2}'`

if [ -n "$ORACLE_HOME" ]; then
  log "$ORACLE_HOME"
  run opatch lsinventory -detail

  separator "#ID#${__name}#1.1.1"
  run grep -i secure_control $ORACLE_HOME/network/admin/listener.ora
 
  separator "#ID#${__name}#1.1.2"
  run grep -i extproc $ORACLE_HOME/network/admin/listener.ora

  separator "#ID#${__name}#1.1.3"
  run grep -i admin_restrictions $ORACLE_HOME/network/admin/listener.ora

  separator "#ID#${__name}#1.1.4"
  run grep 1521 $ORACLE_HOME/network/admin/listener.ora

  separator "#ID#${__name}#1.1.5"
  run grep -i secure_register $ORACLE_HOME/network/admin/listener.ora


  # ---------------------------------------------------------------------------
  separator "#ID#${__name}#1.2.1,1.2.8"
  run ls -alR $ORACLE_HOME

  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#1.2.2"
  # TODO: Find out how to get real oracle login name
  ORACLE_LOCKED=`grep "^${ORACLE_LOGIN}:" /etc/passwd | awk -F":" '{print $2}'`
  if [ "x${ORACLE_LOCKED}" == "xx" ]; then
    fail "#ID#${__name}#1.2.2"
  else
    pass "#ID#${__name}#1.2.2"
  fi

  # ---------------------------------------------------------------------------
  # Do not name "oracle" the oracle account
  #separator "#ID#${__name}#1.2.3"
  grep "^oracle:.*" /etc/passwd &>/dev/null
  if [ $? -eq 0 ]; then
    fail "#ID#${__name}#1.2.3"
  else
    pass "#ID#${__name}#1.2.3"
  fi

  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#1.2.7"
  ORACLE_UMASK=`su - ${ORACLE_LOGIN} -c umask 2>/dev/null`
  if [ $? -eq 0 ] && [ ${ORACLE_UMASK} -lt 0022 ]; then
    fail "#ID#${__name}#1.2.7#${ORACLE_UMASK}"
  else
    pass "#ID#${__name}#1.2.7#${ORACLE_UMASK}"
  fi

  # ---------------------------------------------------------------------------
  separator "#ID#${__name}#1.2.10"
  run "su - ${ORACLE_LOGIN} -c env | grep '^TEMP='"
  run "su - ${ORACLE_LOGIN} -c env | grep '^TMPDIR='"

  # ---------------------------------------------------------------------------
  # separator "#ID#${__name}#1.2.13"
  ORACLE_ROOTG=`grep "^root:" /etc/group | awk -F":" '{print $4}'`
  echo $ORACLE_ROOTG | grep $ORACLE_LOGIN &>/dev/null
  if [ $? -eq 0 ]; then
    fail "#ID#${__name}#1.2.13#${ORACLE_ROOTG}"
  else
    pass "#ID#${__name}#1.2.13#${ORACLE_ROOTG}"
  fi

  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#1.2.14"
  ORACLE_DBAG=`grep "^dba:" /etc/group`
  if [ $? -eq 0 ]; then
    fail "#ID#${__name}#1.2.14#${ORACLE_DBAG}"
  else
    pass "#ID#${__name}#1.2.14#${ORACLE_DBAG}"
  fi

  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#1.2.17"
  ORACLE_TKPROF=`ls -l ${ORACLE_HOME}/bin/tkprof 2>/dev/null`
  if [ $? -eq 0 ]; then
    fail "#ID#${__name}#1.2.17#${ORACLE_TKPROF}"
  else
    pass "#ID#${__name}#1.2.17#${ORACLE_TKPROF}"
  fi

  # ---------------------------------------------------------------------------
  separator "#ID#${__name}#1.2.34"
  find $ORACLE_HOME -perm -4000 -o -perm -2000 -print 2>/dev/null

  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"
  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#"

  # ---------------------------------------------------------------------------
  #separator "#ID#${__name}#4.1.1"
  ORACLE_ORAF=`df --local -P 2>/dev/null | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -iname 'init*.ora' \) 2>/dev/null`
  echo "$ORACLE_ORAF" | while read orafile; do download_clear $orafile; done
else
  info "Oracle home \"\$ORACLE_HOME\" is not set, skipped"
  rm -f $LOG_FILE
fi

# The end of the file oracle.sh
