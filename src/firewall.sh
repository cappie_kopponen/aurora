#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   firewall.sh
# Encoding:    UTF-8
# Description: Firewall
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
# 4.7 Enable firewalld (Scored) - part of network script

# Are iptables started during boot?
redhat v4 v5 v6 eval __lvl=`runlevel | awk '{print $2}'`; BOOT_ENABLED=`chkconfig --list iptables 2>/dev/null | grep "${__lvl}:on" 2>/dev/null`
redhat v7 eval BOOT_ENABLED=`systemctl is-enabled iptables.service 2>&1`
if [ $? -ne 0 ]; then
  fail "Iptables are not configured to start during boot process" "1"
  info $BOOT_ENABLED
else
  pass "Iptables running during boot process"
fi

################################################################################
### Classic manual audit

# V pripade, ze ide o kernel < 2.4, nebudu tam iptables, ale ipchains, TODO
# if kernel < 2.4, do ipchains
# else

# ------------------------------------------------------------------------------
# List all rules in iptables
separator "Firewall rules"
run iptables --list --verbose -n

separator "IPv6 Firewall rules"
run ip6tables --list --verbose -n

# ------------------------------------------------------------------------------
# Download all configuration files
download_clear /etc/sysconfig/iptables
download_clear /etc/sysconfig/ip6tables
download_clear /etc/sysconfig/iptables-config
download_clear /etc/sysconfig/ip6tables-config

separator "FirewallD is active?"
# If this is RHEL>7.0 we should consider FirewallD
if iscommand 'firewall-cmd'; then

  run firewall-cmd --state; ret=$?
  if [ $ret -eq 0 ]; then
  # Ey Carramba! We have FirewallD!
  separator "Firewall default zone and status"
  run firewall-cmd --get-default-zone
  # Not necessary tagging for other operating systems so far it is only on RH7 systems
  if iscommand 'systemctl'; then run systemctl status firewalld; fi
  separator "Firewall active zones"
  run firewall-cmd --get-active-zones

  #Get all configuration within the /etc/firewalld directory?
  download_clear /etc/firewalld/firewalld.conf 

  separator "Get default configuration:"
  run firewall-cmd --list-all
  separator "Get the list of services"
  run firewall-cmd --list-services
  separator "Get the list of ports"
  run firewall-cmd --list-ports
  separator "Get the direct rules (should be empty)"
  run firewall-cmd --direct --get-all-rules
  fi
else
  log "No firewall-cmd command, skipping"
fi

# The end of the file firewall.sh
