#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   snmp.sh
# Encoding:    UTF-8
# Description: SNMP
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy
# Ommited checks due to redundancy

################################################################################
### Classic manual audit

# TODO: Get the file for audit /etc/audit/audit.rules which is default audit
# rules file
SNMPD_FILE="/etc/snmp/snmpd.conf"

download_clear $SNMPD_FILE 

separator "SNMP processes:"
run "ps aux | grep snmp"

################################################################################
### TODO
### Audit SNMP v pripade, ze bezi HP SNMP

# The end of the file snmp.sh
