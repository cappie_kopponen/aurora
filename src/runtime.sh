#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   runtime.sh
# Encoding:    UTF-8
# Description: Runtime information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# 1.6.1 Restrict Core Dumps
run grep 'hard core' /etc/security/limits.conf /etc/security/limits.d/* | grep -v ":#.*"
sysctl fs.suid_dumpable 2>/dev/null | grep "= 0" &>/dev/null; ret=$?
if [ $ret -ne 0 ]; then
  fail "fs.suid_dumpable is enabled" "1"
else
  pass "fs.suid_dumpable is disabled"
fi

# ------------------------------------------------------------------------------

################################################################################
### Classic manual audit

# Run lsof command if available
# ------------------------------------------------------------------------------
if iscommand 'lsof'; then
  separator "Output of lsof"
  lsof_parm="-i"
  solaris lsof_parm="${lsof_parm} -C"
  run lsof $lsof_parm
fi

# Get all kernel modules
# ------------------------------------------------------------------------------
if iscommand 'lsmod'; then
  separator "Output of lsmod"
  run lsmod
fi

# ------------------------------------------------------------------------------
separator "Output of ps"
run ps -ef

# The end of the file runtime.sh
