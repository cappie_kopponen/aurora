#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   basic.sh
# Encoding:    UTF-8
# Description: Basic system information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
#

################################################################################
### Classic manual audit

# TODO: Get the file for audit /etc/audit/audit.rules which is default audit
# rules file
# ------------------------------------------------------------------------------
# Get the default system information
separator "Basic system information"
run 'uname -a'
redhat run cat /etc/redhat-release
solaris run cat /etc/release

# ------------------------------------------------------------------------------
# Get the default network interface
separator "IP address of default interface (from route information)"
DEFAULT_NINT=`route 2>/dev/null | grep default | awk '{print $NF}'`

if [ -z "$DEFAULT_NINT" ]; then
  if iscommand 'netstat'; then
    redhat eval DEFAULT_NINT=$(netstat -rn4 | egrep "^default|^0\.0\.0\.0" | awk '{print $NF}')
    solaris eval DEFAULT_NINT=$(netstat -rf inet | egrep "^default|^0\.0\.0\.0" | awk '{print $NF}')
  fi
fi

IPS=`ifconfig $DEFAULT_NINT | grep inet | awk '{print $2}'`
for ip in $IPS; do log -n "IP address: $ip"; done

# ------------------------------------------------------------------------------
separator "Hostname: "
run hostname

separator "Running as user: $WHOAMI"

separator "Actuall time status:"
if iscommand 'timedatectl'; then run timedatectl status; fi

separator "Runlevel:"
if iscommand 'runlevel'; then run runlevel; fi

separator "Umask of user $WHOAMI"
if iscommand 'umask'; then run umask; fi

# ------------------------------------------------------------------------------
# On RedHat 7 we have target, let's determine which target is default, current
# and what is running right now
redhat v7 separator "Default target"
redhat v7 run systemctl get-default

# For each target unit, this commands displays its full name (UNIT) followed by
# a note whether the  unit has  been loaded (LOAD), its high-level (ACTIVE) and
# low-level (SUB) unit activation state, and a short description (DESCRIPTION).
redhat v7 separator "Display all units"
redhat v7 run systemctl list-units --type target --all

# To display an overview of overridden or modified unit files
redhat v7 separator "An overview of overridden or modified unit files"
redhat v7 run systemd-delta

# The end of the file basic.sh
