#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   software.sh
# Encoding:    UTF-8
# Description: Software information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# TODO:
# 1.2.1 Configure Connection to the RHN RPM Repositories (Not Scored)
# 1.2.5 Obtain Software Package Updates with yum (Not Scored)
# Pozn. Toto sa kontroluje pomocou "# yum check-update", je OK pustit tento 
#       update? Moze nejak menit subory na systemy? rpm-db?
#
# ------------------------------------------------------------------------------

# 1.2.2 Verify Red Hat GPG Key is Installed
redhat separator "Verify Red Hat GPG Key is installed [CIS RHEL7 1.0.0]"
redhat run 'rpm -q --queryformat "%{SUMMARY}\n" gpg-pubkey'

# ------------------------------------------------------------------------------
# 1.2.3 Verify that gpgcheck is Globally Activated
redhat grep gpgcheck /etc/yum.conf | grep "gpgcheck=1" &>/dev/null
redhat eval [[ $? -ne 0 ]] && fail "RPM package's signature is not checked prior to its installation" "1" || pass "RPM package's signature is checked prior to its installation"

# ------------------------------------------------------------------------------
# 1.2.4 Disable the rhnsd Daemon (Not Scored)
redhat v7       read RHNSD_SERVICE < <(systemctl -q is-enabled rhnsd 2>&1)
redhat v4 v5 v6 read RHNSD_SERVICE < <(/sbin/service rhnsd status 2>&1)
redhat eval [[ $? -eq 0 ]] && fail "Automatic deployment of packages may be active" "2" && log "$RHNSD_SERVICE" || pass "Daemon rhnsd is disabled"

# ------------------------------------------------------------------------------
# 1.2.6 Verify Package Integrity Using RPM (Not Scored)
#echo -n "Checking packages integrity (can take up to few minutes) ... " # Not supporting right now
redhat SOFTWARE_INTEGRITY=`rpm -qVa 2>&1 | awk '$2 != "c" { print $0 }'` &>/dev/null
# echo $SOFTWARE_INTEGRITY
#; clear
if [ -n "$SOFTWARE_INTEGRITY" ]; then
  fail "File integrity issue with installed packages" "3"
  while read -r pkg; do
    log -n "$pkg"; 
  done <<< "$SOFTWARE_INTEGRITY"
else
  pass "File integrity of all packages passed"
fi

# ------------------------------------------------------------------------------
# 1.3.1 Install AIDE (Scored)
package=$(isinstalled aide)
if [ $? -ne 0 ]; then
  fail "AIDE software is not installed" "4"
else
  pass "AIDE software is installed: $package"
  # ----------------------------------------------------------------------
  # 1.3.2 Implement Periodic Execution of File Integrity (Scored)
  SOFTWARE_AIDE=`crontab -u root -l 2>/dev/null | grep aide`
  if [ $? -ne 0 ]; then
    fail "No crontab job for AIDE periodic check" "5"
    download /etc/crontab
  else
    pass "AIDE is scheduled for periodic check"
    log -n "$SOFTWARE_AIDE"
  fi
fi

# ------------------------------------------------------------------------------
# 2.1.1 - 2.1.11 Remove unnecessary software
# 3.5 Remove DHCP Server
# 3.7 Remove LDAP (Not Scored)
# 3.9 - 3.15 Remove DNS Server, FTP Server, HTTP Server, Dovecot, Samba, 
# Squid Proxy, SNMP Server (Not Scored)
PROCESSES_LIST_PKGS="telnet-server telnet rsh-server rsh ypbind ypserv tftp \
         tftp-server talk talk-server xinetd dhcp openldap-servers \
         openldap-clients bind vsftpd httpd dovecot samba squid \
         net-snmp git subversion"
for process in $(echo $PROCESSES_LIST_PKGS); do
  package=$(isinstalled $process); ret=$?
  if [ $ret -eq 0 ]; then
    fail "Package \"$process\" is installed: \"$package\"" "6"
  else
    pass "Package \"$process\" is not installed"
  fi
done

################################################################################
### Classic manual audit

redhat separator "Find all patches"
redhat run "rpm -qa --queryformat '%{installtime} (%{installtime:date}) %{name}\n' | sort -n"

separator "Forbidden packages installed:"
SOFTWARE_FORBIDDEN="make gcc telnet tftp gdb nmap lynx links"
redhat run "rpm -q $SOFTWARE_FORBIDDEN | grep -v 'is not installed'"

separator "All installed packages:"
redhat   run rpm -qa
solaris  run "showrev -p"

redhat separator "Information about yum repositories"
redhat v7         run yum repoinfo all
redhat v4 v5 v6   run yum repolist -v

separator "Audit installed packages"
solaris run "pkgchk -n 2>&1"

# The end of the file software.sh
