#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   network.sh
# Encoding:    UTF-8
# Description: Network information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
#

# ------------------------------------------------------------------------------
# 4.1.1 Disable IP Forwarding
redhat eval NETWORK_01=`/sbin/sysctl net.ipv4.ip_forward | awk -F"= *" '{print $2}'`
if [[ $NETWORK_01 -eq 1 ]]
then    fail "Packet forwarding net.ipv4.ip_forward is set to \"$NETWORK_01\"" "1"
else    pass "Packet forwarding net.ipv4.ip_forward is set to \"$NETWORK_01\""
fi

# ------------------------------------------------------------------------------
# 4.1.2 Disable Send Packet Redirects
redhat eval NETWORK_02=`/sbin/sysctl net.ipv4.conf.all.send_redirects | awk -F"= *" '{print $2}'`
redhat eval NETWORK_03=`/sbin/sysctl net.ipv4.conf.default.send_redirects | awk -F"= *" '{print $2}'`
if [[ $NETWORK_02 -eq 1 ]]; then
  fail "Packet forwarding net.ipv4.conf.all.send_redirects is set to \"$NETWORK_02\"" "2"
else
  pass "Packet forwarding net.ipv4.conf.all.send_redirects is set to \"$NETWORK_02\""
fi
if [[ $NETWORK_03 -eq 1 ]]; then
  fail "Packet forwarding net.ipv4.conf.default.send_redirects is set to \"$NETWORK_03\"" "3"
else
  pass "Packet forwarding net.ipv4.conf.default.send_redirects is set to \"$NETWORK_03\""
fi

# ------------------------------------------------------------------------------
# 4.2.1 Disable Source Routed Packet Acceptance
redhat eval NETWORK_04=`/sbin/sysctl net.ipv4.conf.all.accept_source_route | awk -F"= *" '{print $2}'`
redhat eval NETWORK_05=`/sbin/sysctl net.ipv4.conf.default.accept_source_route | awk -F"= *" '{print $2}'`
if [[ $NETWORK_04 -eq 1 ]]; then
  fail "Routed Packet Acceptance net.ipv4.conf.all.accept_source_route is set to \"$NETWORK_04\"" "4"
else
  pass "Routed Packet Acceptance net.ipv4.conf.default.accept_source_route is set to \"$NETWORK_04\""
fi
if [[ $NETWORK_05 -eq 1 ]]; then
  fail "Routed Packet Acceptance net.ipv4.conf.default.send_redirects is set to \"$NETWORK_05\"" "5"
else
  pass "Routed Packet Acceptance net.ipv4.conf.default.send_redirects is set to \"$NETWORK_05\""
fi

# ------------------------------------------------------------------------------
# 4.2.3 Disable Secure ICMP Redirect Acceptance
redhat eval NETWORK_06=`/sbin/sysctl net.ipv4.conf.all.secure_redirects | awk -F"= *" '{print $2}'`
redhat eval NETWORK_07=`/sbin/sysctl net.ipv4.conf.default.secure_redirects | awk -F"= *" '{print $2}'`
if [[ $NETWORK_06 -eq 1 ]]; then
  fail "Redirect Acceptance net.ipv4.conf.all.secure_redirects is set to \"$NETWORK_06\"" "6"
else
  pass "Redirect Acceptance net.ipv4.conf.all.secure_redirects is set to \"$NETWORK_06\""
fi
if [[ $NETWORK_07 -eq 1 ]]; then
  fail "Redirect Acceptance net.ipv4.conf.default.secure_redirects is set to \"$NETWORK_07\"" "7"
else
  pass "Redirect Acceptance net.ipv4.conf.default.secure_redirects is set to \"$NETWORK_07\""
fi

# ------------------------------------------------------------------------------
# 4.2.4 Log Suspicious Packets
redhat eval NETWORK_08=`/sbin/sysctl net.ipv4.conf.all.log_martians | awk -F"= *" '{print $2}'`
redhat eval NETWORK_09=`/sbin/sysctl net.ipv4.conf.default.log_martians | awk -F"= *" '{print $2}'`
if [[ $NETWORK_08 -eq 1 ]]; then
  fail "Suspicious Packets net.ipv5.conf.all.log_martians is set to \"$NETWORK_08\"" "8"
else
  pass "Suspicious Packets net.ipv4.conf.all.log_martians is set to \"$NETWORK_08\""
fi
if [[ $NETWORK_09 -eq 1 ]]; then
  fail "Suspicious Packets net.ipv4.conf.default.log_martians is set to \"$NETWORK_09\"" "9"
else
  pass "Suspicious Packets net.ipv4.conf.default.log_martians is set to \"$NETWORK_09\""
fi

# ------------------------------------------------------------------------------
# 4.2.5 Enable Ignore Broadcast Requests 
redhat eval NETWORK_10=`/sbin/sysctl net.ipv4.icmp_echo_ignore_broadcasts | awk -F"= *" '{print $2}'`
if [[ $NETWORK_10 -ne 1 ]]; then
  fail "Ignore Broadcast net.ipv4.icmp_echo_ignore_broadcasts is set to \"$NETWORK_10\"" "10"
else
  pass "Ignore Broadcast net.ipv4.icmp_echo_ignore_broadcasts is set to \"$NETWORK_10\""
fi

# ------------------------------------------------------------------------------
# 4.2.6 Enable Bad Error Message Protection 
redhat eval NETWORK_11=`/sbin/sysctl net.ipv4.icmp_ignore_bogus_error_responses | awk -F"= *" '{print $2}'`
if [[ $NETWORK_11 -eq 1 ]]; then
  fail "Bad Error Message Protection net.ipv4.icmp_ignore_bogus_error_responses is set to \"$NETWORK_11\"" "11"
else
  pass "Bad Error Message Protection net.ipv4.icmp_ignore_bogus_error_responses is set to \"$NETWORK_11\""
fi

# ------------------------------------------------------------------------------
# 4.2.7 Enable RFC-recommended Source Route Validation
redhat eval NETWORK_12=`/sbin/sysctl net.ipv4.conf.all.rp_filter | awk -F"= *" '{print $2}'`
redhat eval NETWORK_13=`/sbin/sysctl net.ipv4.conf.default.rp_filter | awk -F"= *" '{print $2}'`
if [[ $NETWORK_12 -eq 0 ]]; then
  fail "RFC-recommended Source Route Validation net.ipv4.conf.all.rp_filter is set to \"$NETWORK_12\"" "12"
else
  pass "RFC-recommended Source Route Validation net.ipv4.conf.all.rp_filter is set to \"$NETWORK_12\""
fi
if [[ $NETWORK_13 -eq 0 ]]; then
  fail "RFC-recommended Source Route Validation net.ipv4.conf.default.rp_filter is set to \"$NETWORK_13\"" "13"
else
  pass "RFC-recommended Source Route Validation net.ipv4.conf.default.rp_filter is set to \"$NETWORK_13\""
fi

# ------------------------------------------------------------------------------
# 4.2.8 Enable TCP SYN Cookies
redhat eval NETWORK_14=`/sbin/sysctl net.ipv4.tcp_syncookies | awk -F"= *" '{print $2}'`
if [[ $NETWORK_14 -eq 1 ]]; then
  fail "TCP SYN Cookies net.ipv4.tcp_syncookies is set to \"$NETWORK_14\"" "14"
else
  pass "TCP SYN Cookies net.ipv4.tcp_syncookies is set to \"$NETWORK_14\""
fi

# ------------------------------------------------------------------------------
# 4.5.1 Install TCP Wrappers (Not Scored)
ret=-1
package=$(isinstalled tcp_wrappers-libs); ret=$?
if [ $ret -eq 0 ]; then
  pass "TCP Wrappers are installed \"$package\""
else
  fail "TCP Wrappers are not installed" "15"
fi

# ------------------------------------------------------------------------------
# 4.5.3 Verify Permissions on /etc/hosts.allow
redhat eval NETWORK_16=`stat -c %a /etc/hosts.allow`
if [[ $NETWORK_15 -ne 644 ]]; then
  fail "Permissions on /etc/hosts.allow are set to \"$NETWORK_15\"" "16"
else
  pass "Permissions on /etc/hosts.allow are set to \"$NETWORK_15\""
fi

# ------------------------------------------------------------------------------
# 4.5.3 Verify Permissions on /etc/hosts.deny
redhat eval NETWORK_17=`stat -c %a /etc/hosts.allow`
if [[ $NETWORK_16 -ne 644 ]]; then
  fail "Permissions on /etc/hosts.deny are set to \"$NETWORK_16\"" "17"
else
  pass "Permissions on /etc/hosts.deny are set to \"$NETWORK_16\""
fi


# ------------------------------------------------------------------------------
# 4.6.1 Disable DCCP (Not Scored)
# 4.6.2 Disable SCTP (Not Scored)
# 4.6.3 Disable RDS (Not Scored)
# 4.6.4 Disable TIPC (Not Scored)

# ==============================================================================
# Platform specific CIS checks
# ==============================================================================
# 4.7 Enable firewalld (Scored)
redhat v7 run "systemctl -q is-enabled firewalld"
redhat v7 eval NETWORK_17=$?
redhat v7 eval [[ $NETWORK_17 -ne 0 ]] && fail "Firewalld is not enabled" "17" || pass "Firewalld is enabled"

################################################################################
### Classic manual audit

# ------------------------------------------------------------------------------
# Route table
separator "Routing"

run route
run ip route show

# ------------------------------------------------------------------------------
# Information about interfaces
separator "Networking"

run ifconfig -a
run ip addr show

download_clear "/etc/hosts"
download_clear "/etc/hosts.allowa"
download_clear "/etc/hosts.deny"
download_clear "/etc/resolv.conf"

# The end of the file network.sh
