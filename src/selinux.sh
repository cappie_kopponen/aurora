#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   selinux.sh
# Encoding:    UTF-8
# Description: Selinux
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Get selinux status -----------------------------------------------------------
separator "Selinux status"

iscommand getenforce
if [ $? -eq 0 ]; then
  run /usr/sbin/getenforce
  SELINUX_MODE=`/usr/sbin/getenforce`

  if [ $SELINUX_MODE != "Enforcing" ]; then
    fail "Selinux is in \"$SELINUX_MODE\" mode." "1"
  else
    pass "Selinux is in \"$SELINUX_MODE\" mode."
  fi

  # ------------------------------------------------------------------------------
  # Record Events that Modify the System's Mandatory Access Controls
  # CCE-26657-7
  # Add the following to "/etc/audit/audit.rules": -w /etc/selinux/ -p wa -k MAC-policy
  SELINUX_MOD_AC=`egrep "^\-w\s+/etc/selinux/\s+\-p\s+wa\s+\-k\s+[-\w]+\s*$" /etc/audit/audit.rules`; ret=$?
  if [ $ret -eq 0 ]; then
    fail "The audit system is not configured to audit modifications to the SELinux" "2"
  elif [ $ret -eq 2 ]; then
    error "Greping /etc/audit/audit.rules failed. Downloading audit.rules for inspection."
    download_clear /etc/audit/audit.rules
  else
    pass "The audit system is properly configured for auditing modifications to the SELinux"
  fi

  # ------------------------------------------------------------------------------
  # The system must use a Linux Security Module at boot time
  # Check value selinux|enforcing=0 in /etc/grub.conf, fail if found
  # CCE-26956-3
  SELINUX_BOOT_ALLOW=`egrep "^[\s]*kernel[\s]+.*(selinux|enforcing)=0.*$" /boot/grub2/grub.cfg`; ret=$?
  if [ $ret -eq 1 ]; then
    fail "The system does not use a SELinux at boot time" "3"
  elif [ $ret -eq 2 ]; then
    error "Greping /etc/grub.conf failed."
    download_clear /boot/grub2/grub.cfg
  else
    pass "SELinux is enabled at boot time"
  fi

  # ------------------------------------------------------------------------------
  # 1.4.2 Set the SELinux State
  grep SELINUX=enforcing /etc/selinux/config &>/dev/null
  if [ $? -ne 0 ]; then
    fail "SELINUX is not enabled at boot time" "4"
  else
    pass "RPM package's signature is checked prior to its installation"
  fi

  # ------------------------------------------------------------------------------
  # 1.4.3 Set the SELinux Policy
  grep "SELINUXTYPE=targeted" /etc/selinux/config &>/dev/null
  if [ $? -ne 0 ]; then
    fail "Targeted policy is not selected (SELINUX)" "5"
  else
    pass "Targeted policy selected (SELINUX)"
  fi

  #------------------------------------------------------------------------------
  # 1.4.4 Remove SETroubleshoot (Scored)
  package=$(isinstalled setroubleshoot)
  if [ $? -eq 0 ]; then
    fail "SETroubleshoot is installed: \'$package\'" "6"
  else
    pass "SETroubleshoot is not installed"
  fi

  #------------------------------------------------------------------------------
  # 1.4.5 Remove MCS Translation Service (mcstrans)
  package=$(isinstalled mcstrans)
  if [ $? -eq 0 ]; then
    fail "MCS Translation service is installed: \'$package\'" "7"
  else
    pass "MCS Translation service is not installed"
  fi

  #------------------------------------------------------------------------------
  # 1.4.6 Check for Unconfined Daemons
  SELINUX_UNCONFINED_DAEMONS=`ps -eZ | egrep "initrc" | egrep -vw "tr|ps|egrep|bash|awk" | tr ':' ' ' | awk '{print $NF }'`
  if [ -n "$SELINUX_UNCONFINED_DAEMONS" ]; then
    fail "Unconfined daemons running on the system (SELINUX)" "8"
    log $SELINUX_UNCONFINED_DAEMONS
  else
    pass "No unconfined daemons found on the system (SELINUX)"
  fi

################################################################################
### Classic manual audit

  # Get all sebool variables -----------------------------------------------------
  #SELINUX_VARIABLES=`/usr/sbin/getsebool -a`

  # ------------------------------------------------------------------------------
  #TODO
  separator "SELinux mode and SELinux policy in use"
  run sestatus -v

  # ------------------------------------------------------------------------------
  separator "Context information associated with processes"
  run ps -Z

  # ------------------------------------------------------------------------------
  separator "Performing diagnostic"
  run avcstat

  if iscommand 'semanage'; then
    # ----------------------------------------------------------------------
    separator "Listing user mappings"
    run semanage login -l

    # ----------------------------------------------------------------------
    separator "Listing SELinux boolean values"
    # Does not work on RHEL 5 - TODO
    redhat v4 v5 run 'getsebool -a'
    redhat v6 v7 run 'semanage boolean -l'
  fi

  download /etc/selinux/config

else
  info "SELinux was not found on the system, skipped"
  rm -f $LOG_FILE
fi

# The end of the file selinux.sh
