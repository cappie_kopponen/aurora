#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   mail.sh
# Encoding:    UTF-8
# Description: Mail service
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy
# Ommited checks due to redundancy:

################################################################################
### Classic manual audit
### SENDMAIL

if [ -f "/etc/sendmail.cf" ]; then download_clear "/etc/sendmail.cf"; fi
if [ -f "/etc/mail/sendmail.cf" ]; then download_clear "/etc/mail/sendmail.cf"; fi
if [ -f "/etc/mail/relay-domains" ]; then download_clear "/etc/mail/relay-domains"; fi

separator "Sendmail processes:"
run "ps aux | grep sendmail"

separator "Checking mailhost alias in /etc/hosts"
run "grep mailhost /etc/hosts"

################################################################################
### POSTFIX

if iscommand "postfix"; then
  separator "Postfix processes:"
  run "ps aux | grep smtpd"

  separator "Postfix non default settings"
  run postconf -n 2>&1

  separator "Postfix permissions"
  for f in `whereis postfix | awk -F: '{print $2}'`; do 
    log -n $(ls -ld $f); 
  done

  # This is check, but will also create missing directories, which will change the system during audit!!
  #run postfix -v -v check 2>&1
  POSTFIX_PREFIXES="/etc/postfix /usr/local/postfix"
  POSTFIX_FILES="main.cf master.cf canonical recipient_canonical access virtual transport relocated"
  for prefix in $POSTFIX_PREFIXES; do
    for file in $POSTFIX_FILES; do
      if [ -f "${prefix}/${file}" ]; then download_clear "${prefix}/${file}"; fi
    done
  done
fi

# The end of the file mail.sh
