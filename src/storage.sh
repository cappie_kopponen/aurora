#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   storage.sh
# Encoding:    UTF-8
# Description: Disk information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

################################################################################
### Audit against CIS sec policy

# Ommited checks due to redundancy:
#

################################################################################
### Classic manual audit

# ------------------------------------------------------------------------------
# Disk usage information
separator "Disk usage"
run df -h

# ------------------------------------------------------------------------------
# Mount information
separator "Disk mount options"
run mount -l
download_clear /etc/fstab

# The end of the file storage.sh
