#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   logs.sh
# Encoding:    UTF-8
# Description: Log information
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

LOG_CONF="/etc/systemd/journald.conf"

################################################################################
### Audit against CIS sec policy

# ------------------------------------------------------------------------------
# 5.1.1 Install the rsyslog package
LOGS_01=$(isinstalled rsyslog)
if [ $? -ne 0 ]; then
  fail "Rsyslog package is not installed: \"$LOGS_01\"" "1"
else
  pass "Rsyslog package is installed: \"$LOGS_01\""
fi

# ------------------------------------------------------------------------------
# 5.1.2 Activate the rsyslog Service
LOGS_02=3
redhat v4 v5 v6  run "service syslog status"
redhat v7        run "systemctl -q is-enabled rsyslog"
redhat eval LOGS_02=$?
if [ $LOGS_02 -eq 3 ]; then
  error "Not able to check syslog is running"
elif [ $LOGS_02 -ne 0 ]; then
  fail "Syslog is not enabled" "2"
else
  pass "Syslog is enabled"
fi

# ------------------------------------------------------------------------------
# 5.1.4 Create and Set Permissions on rsyslog Log Files

# ------------------------------------------------------------------------------

################################################################################
### Classic manual audit

# Process all logs
redhat v7 download_clear $LOG_CONF
redhat v7 separator "All logs with warning or higher severity"
redhat v7 run journalctl -a -l -p warning

# List permissions of logs
run ls -R -la -h /var/log

# The end of the file logs.sh
