#!/usr/bin/env bash
#
# Project:     Tool for Security Auditing of Linux/Unix/AIX OS
# Author:      Koppon Martin        xkoppo00@stud.fit.vutbr.cz
# File name:   template.sh
# Encoding:    UTF-8
# Description: Template of script in "./src" directory
#

. ./lib/default_variables.sh 
. ./lib/error_mssgs.sh 
. ./lib/functions.sh
. ./lib/lib_solaris.sh
. ./lib/lib_redhat.sh

TEMPLATE_CONF=

################################################################################

redhat           echo "Template - example of running command on all version of Red Hat Enterprise Linux"

solaris          echo "Template - example of running command on all version of Solaris"

redhat v4 v5 v6  echo "Template - example of running command only on Red Hat Enterprise Linux in version 4, 5 and 6"

solaris v11      echo "Template - example of running command only on Solaris in version 11"

################################################################################
### Audit against CIS sec policy

TAMPLATE_RESTICTIONS=`grep "restrict" $TEMPLATE_CONF 2>/dev/null`; ret=$?
if [ $ret -eq 1 ] ; then
  fail "Fail text"
elif [ $ret -eq 2 ] ; then
  error "Something failed, probably the file does not exist, let's check"
  redhat rpm -q ntp
  solaris pkginfo | grep -i ntp
else
  pass "Grep failed to find what you are looking"
fi

################################################################################
### Classic manual audit

# NTP configuration
separator "Describe what happens here"
if iscommand 'echo'; then

  separator "Let's download something without comments"
  download_clear $TEMPLATE_CONF

  separator "Let's run some command"
  run ntpq -p
else
  log "No echo service detected"
fi

# The end of the file template.sh
